/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GPIO_H
#define _GPIO_H

#include "gpioutil.h"

// Initialize all GPIO pins to their default function and set the data bus to
// read mode (hi-z).
void gpio_init();

// Important: D10/PB2/RD_WR is the SS (peripheral select) pin and must always be an output when SPI is used.
// This ensures that it doesn't trigger the SPI bus contention circuitry.
// The logic level doesn't matter, so the pin can be used for other purposes (as long as it's an output).

#if BOARD_REV == 5

#define gpio_sdcs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D10)
#define gpio_sdcs_clear() GPIOU_SET(PORT, GPIOU_UNO_D10)
#define gpio_sdcs_init() GPIOU_OUT_HIGH(GPIOU_UNO_D10)
#define gpio_ycs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_A5)
#define gpio_ycs_clear() GPIOU_SET(PORT, GPIOU_UNO_A5)
#define gpio_ycs_init() GPIOU_OUT_HIGH(GPIOU_UNO_A5)
#define gpio_sdoe_set() GPIOU_SET(PORT, GPIOU_UNO_D8)
#define gpio_sdoe_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D8)
#define gpio_sdoe_init() GPIOU_OUT_LOW(GPIOU_UNO_D8)
#define gpio_yoe_set() GPIOU_NOP()
#define gpio_yoe_clear() GPIOU_NOP()
#define gpio_yoe_init() GPIOU_NOP()
#define gpio_rdwr_write() GPIOU_CLEAR(PORT, GPIOU_UNO_A4)
#define gpio_rdwr_read() GPIOU_SET(PORT, GPIOU_UNO_A4)
#define gpio_rdwr_init() GPIOU_OUT_HIGH(GPIOU_UNO_A4)
#define gpio_sck_set() GPIOU_SET(PORT, GPIOU_UNO_D13)
#define gpio_sck_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D13)
#define gpio_sck_init() GPIOU_OUT_LOW(GPIOU_UNO_D13)
#define gpio_miso_init() GPIOU_IN_HIZ(GPIOU_UNO_D12)
#define gpio_mosi_init() GPIOU_OUT_HIGH(GPIOU_UNO_D11)
#define gpio_a0_high() GPIOU_SET(PORT, GPIOU_UNO_A3)
#define gpio_a0_low() GPIOU_CLEAR(PORT, GPIOU_UNO_A3)
#define gpio_a0_init() GPIOU_OUT_LOW(GPIOU_UNO_A3)
#define gpio_a1_high() GPIOU_SET(PORT, GPIOU_UNO_A2)
#define gpio_a1_low() GPIOU_CLEAR(PORT, GPIOU_UNO_A2)
#define gpio_a1_init() GPIOU_OUT_LOW(GPIOU_UNO_A2)
#define gpio_sddet_get() (GPIOU_GET(PIN, GPIOU_UNO_D9) == 0)
#define gpio_sddet_init() GPIOU_IN_HIZ(GPIOU_UNO_D9)
// optimized macros for data blocks
#define gpio_data_out() do { \
	/* A0..A1 */ \
	PORTC &= ~0x03; \
	DDRC |= 0x03; \
	/* D2..D7 */ \
	PORTD &= ~0xfc; \
	DDRD |= 0xfc; \
} while (0)
#define gpio_data_in() do { \
	/* A0..A1 */ \
	PORTC &= ~0x03; \
	DDRC &= ~0x03; \
	/* D2..D7 */ \
	PORTD &= ~0xfc; \
	DDRD &= ~0xfc; \
} while (0)
#define gpio_data_set(bits) do { \
	/* A0..A1 */ \
	PORTC = (PORTC & ~0x03) | (bits & 0x03); \
	/* D2..D7 */ \
	PORTD = (PORTD & ~0xfc) | (bits & 0xfc); \
} while (0)
#define gpio_data_get() ((PINC & 0x03) | (PIND & 0xfc))

#elif BOARD_REV == 4

#define gpio_sdcs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D3)
#define gpio_sdcs_clear() GPIOU_SET(PORT, GPIOU_UNO_D3)
#define gpio_sdcs_init() GPIOU_OUT_HIGH(GPIOU_UNO_D3)
#define gpio_ycs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D4)
#define gpio_ycs_clear() GPIOU_SET(PORT, GPIOU_UNO_D4)
#define gpio_ycs_init() GPIOU_OUT_HIGH(GPIOU_UNO_D4)
#define gpio_sdoe_set() GPIOU_SET(PORT, GPIOU_UNO_D8)
#define gpio_sdoe_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D8)
#define gpio_sdoe_init() GPIOU_OUT_LOW(GPIOU_UNO_D8)
#define gpio_yoe_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D9)
#define gpio_yoe_clear() GPIOU_SET(PORT, GPIOU_UNO_D9)
#define gpio_yoe_init() GPIOU_OUT_HIGH(GPIOU_UNO_D9)
#define gpio_rdwr_write() GPIOU_CLEAR(PORT, GPIOU_UNO_D10)
#define gpio_rdwr_read() GPIOU_SET(PORT, GPIOU_UNO_D10)
#define gpio_rdwr_init() GPIOU_OUT_HIGH(GPIOU_UNO_D10)
#define gpio_sck_set() GPIOU_SET(PORT, GPIOU_UNO_D13)
#define gpio_sck_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D13)
#define gpio_sck_init() GPIOU_OUT_LOW(GPIOU_UNO_D13)
#define gpio_miso_init() GPIOU_IN_HIZ(GPIOU_UNO_D12)
#define gpio_mosi_init() GPIOU_OUT_HIGH(GPIOU_UNO_D11)
#define gpio_a0_high() GPIOU_SET(PORT, GPIOU_UNO_D6)
#define gpio_a0_low() GPIOU_CLEAR(PORT, GPIOU_UNO_D6)
#define gpio_a0_init() GPIOU_OUT_LOW(GPIOU_UNO_D6)
#define gpio_a1_high() GPIOU_SET(PORT, GPIOU_UNO_D5)
#define gpio_a1_low() GPIOU_CLEAR(PORT, GPIOU_UNO_D5)
#define gpio_a1_init() GPIOU_OUT_LOW(GPIOU_UNO_D5)
#define gpio_sddet_get() (GPIOU_GET(PIN, GPIOU_UNO_A0) == 0)
#define gpio_sddet_init() GPIOU_IN_HIZ(GPIOU_UNO_A0)
// not used
#define gpio_data_out() do {} while (0)
#define gpio_data_in() do {} while (0)
#define gpio_data_set(bits) do {} while (0)
#define gpio_data_get() (0)

#elif BOARD_REV == 3

#define gpio_sdcs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D8)
#define gpio_sdcs_clear() GPIOU_SET(PORT, GPIOU_UNO_D8)
#define gpio_sdcs_init() GPIOU_OUT_HIGH(GPIOU_UNO_D8)
#define gpio_ycs_set() GPIOU_NOP()
#define gpio_ycs_clear() GPIOU_NOP()
#define gpio_ycs_init() GPIOU_NOP()
#define gpio_sdoe_set() GPIOU_SET(PORT, GPIOU_UNO_D3)
#define gpio_sdoe_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D3)
#define gpio_sdoe_init() GPIOU_OUT_LOW(GPIOU_UNO_D3)
#define gpio_yoe_set() GPIOU_NOP()
#define gpio_yoe_clear() GPIOU_NOP()
#define gpio_yoe_init() GPIOU_NOP()
#define gpio_rdwr_write() GPIOU_NOP()
#define gpio_rdwr_read() GPIOU_NOP()
#define gpio_rdwr_init() GPIOU_OUT_HIGH(GPIOU_UNO_D10)
#define gpio_sck_set() GPIOU_SET(PORT, GPIOU_UNO_D13)
#define gpio_sck_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D13)
#define gpio_sck_init() GPIOU_OUT_LOW(GPIOU_UNO_D13)
#define gpio_miso_init() GPIOU_IN_HIZ(GPIOU_UNO_D12)
#define gpio_mosi_init() GPIOU_OUT_HIGH(GPIOU_UNO_D11)
#define gpio_a0_high() GPIOU_NOP()
#define gpio_a0_low() GPIOU_NOP()
#define gpio_a0_init() GPIOU_NOP()
#define gpio_a1_high() GPIOU_NOP()
#define gpio_a1_low() GPIOU_NOP()
#define gpio_a1_init() GPIOU_NOP()
#define gpio_sddet_get() (GPIOU_GET(PIN, GPIOU_UNO_A0) == 0)
#define gpio_sddet_init() GPIOU_IN_HIZ(GPIOU_UNO_A0)
// not used
#define gpio_data_out() do {} while (0)
#define gpio_data_in() do {} while (0)
#define gpio_data_set(bits) do {} while (0)
#define gpio_data_get() (0)

#elif BOARD_REV == 255

#define gpio_sdcs_set() GPIOU_CLEAR(PORT, GPIOU_UNO_D8)
#define gpio_sdcs_clear() GPIOU_SET(PORT, GPIOU_UNO_D8)
#define gpio_sdcs_init() GPIOU_OUT_HIGH(GPIOU_UNO_D8)
#define gpio_ycs_set() GPIOU_NOP()
#define gpio_ycs_clear() GPIOU_NOP()
#define gpio_ycs_init() GPIOU_NOP()
#define gpio_sdoe_set() GPIOU_NOP()
#define gpio_sdoe_clear() GPIOU_NOP()
#define gpio_sdoe_init() GPIOU_NOP()
#define gpio_yoe_set() GPIOU_NOP()
#define gpio_yoe_clear() GPIOU_NOP()
#define gpio_yoe_init() GPIOU_NOP()
#define gpio_rdwr_write() GPIOU_NOP()
#define gpio_rdwr_read() GPIOU_NOP()
#define gpio_rdwr_init() GPIOU_OUT_HIGH(GPIOU_UNO_D10)
#define gpio_sck_set() GPIOU_SET(PORT, GPIOU_UNO_D13)
#define gpio_sck_clear() GPIOU_CLEAR(PORT, GPIOU_UNO_D13)
#define gpio_sck_init() GPIOU_OUT_LOW(GPIOU_UNO_D13)
#define gpio_miso_init() GPIOU_IN_HIZ(GPIOU_UNO_D12)
#define gpio_mosi_init() GPIOU_OUT_HIGH(GPIOU_UNO_D11)
#define gpio_a0_high() GPIOU_NOP()
#define gpio_a0_low() GPIOU_NOP()
#define gpio_a0_init() GPIOU_NOP()
#define gpio_a1_high() GPIOU_NOP()
#define gpio_a1_low() GPIOU_NOP()
#define gpio_a1_init() GPIOU_NOP()
#define gpio_sddet_get() (1)
#define gpio_sddet_init() GPIOU_NOP()
// not used
#define gpio_data_out() do {} while (0)
#define gpio_data_in() do {} while (0)
#define gpio_data_set(bits) do {} while (0)
#define gpio_data_get() (0)

#else

#error Unsupported board revision, please define BOARD_REV

#endif

#endif //_GPIO_H
