/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <avr/pgmspace.h>
#include "rdos.h"
#include "clock.h"
#include "ymf.h"

static const PROGMEM char rdos_signature[] = "RAWADATA";

static rdos_read_u16 rdos_read_actual = NULL;
static uint64_t rdos_ptr = 0;
static uint16_t rdos_timeout = 0;
static uint8_t rdos_bank = 0;

static uint16_t rdos_read() {
	uint16_t val = rdos_read_actual(rdos_ptr);
	rdos_ptr += 2;
	return val;
}

static uint16_t rdos_calculate_clock(uint16_t rdosclk) {
	// the correct formula is: rdosclk / (F_OPL / 12) * (F_CPU / CLK_DIVISOR)
	// with F_OPL=14.318180MHz, F_CPU=16MHz, CLK_DIVISOR=64 => divide by 4.772726
	// faster calculation: CLK_DIVISOR=64, divide by 5 => 5% faster
	// with F_OPL=14.318180MHz, F_CPU=16MHz, CLK_DIVISOR=8 => multiply with 1.67619068904
	// faster calculation: CLK_DIVISOR=8, multiply by 2 => 19% faster
#if CLK_DIVISOR==8
	return rdosclk * 2;
#elif CLK_DIVISOR==64
	return rdosclk / 5;
#else
#error Unsupported clock divisor
#endif
}

uint16_t rdos_speed_hz() {
	return (F_CPU / CLK_DIVISOR) / rdos_timeout;
}

bool rdos_init(rdos_read_u16 read) {
	rdos_read_actual = read;
	rdos_ptr = 0;
	rdos_timeout = 0;
	rdos_bank = 0;

	for (uint8_t i = 0; i < 4; i++) {
		uint16_t sig = rdos_read();
		uint16_t correct = pgm_read_word(((uint16_t *) rdos_signature) + i);
		if (sig != correct) {
			return false;
		}
	}

	rdos_timeout = rdos_calculate_clock(rdos_read());

	/*
	Data type 	Name 	Description
	BYTE[8] 	cSignature 	"RAWADATA" (not NULL-terminated)
	UINT16LE 	iClock 	Initial clock speed (can be changed during playback)
	BYTE[] 	cOPLData 	Song data (see below)
	UINT16LE 	iEOF 	0xFFFF to indicate end of song 
	
	Register value 	Purpose
	0x00 	Delay. The data byte is the number of cycles to delay by.
	0x02 	Control data (see below) 

	Control type 	Purpose
	0x00 	Clock change. The following UINT16LE is the new clock speed.
	0x01 	Switch to "low" OPL chip (#0)
	0x02 	Switch to "high" OPL chip (#1)

	iHertz = 1193180.0 / iClockSpeed
	*/

	return true;
}

bool rdos_tick() {
	uint16_t cmd = rdos_read();
	if (cmd == 0xffff) {
		return false;
	}

	uint8_t data = (uint8_t) cmd;
	uint8_t reg = (uint8_t) (cmd >> 8);

	switch (reg) {
		case 0x00:
			// delay
			//printf_P(PSTR("w=%u\n"), data);
			for (uint8_t i = 0; i < data; i++) {
				rdos_wait();
			}
			break;
		case 0x02:
			// control
			switch (data) {
				case 0x00:
					// clock change
					rdos_timeout = rdos_calculate_clock(rdos_read());
					//printf_P(PSTR("t=0x%04x\n"), rdos_timeout);
					break;
				case 0x01:
					// switch to OPL3 register bank 0 (or OPL2 chip 0)
					rdos_bank = 0;
					//puts_P(PSTR("b=0"));
					break;
				case 0x02:
					// switch to OPL3 register bank 1 (or OPL2 chip 1)
					rdos_bank = 1;
					//puts_P(PSTR("b=1"));
					break;
				default:
					// ignore invalid command
					break;
			}
			break;
		default:
			// OPL command
			ymf_write_reg(rdos_bank, reg, data);
			break;
	}

	return true;
}

void rdos_wait() {
	clock_wait(rdos_timeout);
}
