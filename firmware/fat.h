/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FAT_H
#define _FAT_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FAT_BLOCK_SIZE 512UL
#define FAT_MAX_FILE_SIZE (((uint64_t) UINT32_MAX + 1) * FAT_BLOCK_SIZE)

#define FAT_OK 0
#define FAT_ERROR_MBR 1
#define FAT_ERROR_NOEXFAT 3
#define FAT_ERROR_READ 4
#define FAT_ERROR_SECTORSIZE 5
#define FAT_ERROR_TWOFAT 6
#define FAT_ERROR_CHECKSUM 7
#define FAT_ERROR_BADCLUSTER 8
#define FAT_ERROR_OUTOFBOUNDS 9
#define FAT_ERROR_VERSION 10
#define FAT_ERROR_NOTFOUND 11
#define FAT_ERROR_PATHLEN 12
#define FAT_ERROR_INVALIDARG 13
#define FAT_ERROR_INAPPROPRIATE 14
#define FAT_ERROR_NOTIMPLEMENTED 255

typedef uint8_t *(fat_read_block_t)(uint32_t blockid);
typedef uint8_t fat_error_t;

typedef struct {
	uint64_t file_size;
	uint32_t start_cluster;
	uint32_t file_entries;
	bool no_fat_chain;
	struct {
		uint8_t ro:1;
		uint8_t hidden:1;
		uint8_t system:1;
		uint8_t directory:1;
		uint8_t archive:1;
	} flags;
} fat_file_t;

// fat_init initializes internal data structures and reads the MBR and boot block of the exFAT file system.
//
// `read` must be a pointer to a block read function.
//
// Returns an error code if the file system cannot be parsed successfully, FAT_OK otherwise.
fat_error_t fat_init(fat_read_block_t *read);
// fat_open traverses the directory structure to find the specified path.
// A file descriptor is written to `file`, if found.
//
// `path` must be an ISO8859-1 string.
// TODO support unicode
// The lookup is case-sensitive, contrary to usual behavior of Microsoft filesystem drivers.
// Both / and \ are accepted as path separators, and any duplicate or leading separators are ignored.
//
// The path is always treated as absolute. The empty string is equivalent
// to /, \ or a combination thereof and refers to the root of the directory structure.
//
// Can also be used to open a directory entry for listing.
//
// Returns an error code if the file cannot be found, FAT_OK otherwise.
fat_error_t fat_open(const char *path, fat_file_t *file);
// fat_read copies `length` bytes from the file referenced by `file` to `data`,
// starting from byte offset `offset`.
//
// Uses `memcpy()` to do the copying, unless the preprocessor macro FAT_NO_MEMCPY is set.
// In this case, a simple byte-by-byte copy loop is used.
//
// Returns an error code if the specified range cannot be read, FAT_OK otherwise.
// The caller is responsible for specifying a valid range.
fat_error_t fat_read(const fat_file_t *file, uint64_t offset, uint16_t length, void *data);
// fat_list reads the directory entry `index` from an opened directory and
// stores the filename into `filename`, including a null terminator.
// The size of `filename` must be specified in `namelen`.
// The filename is converted by simple truncation to 8 bits from the UTF-16 string in the FAT.
// This will produce a correct representation of the real filename, as long as it only
// uses Latin-1 characters.
// A null terminator is always stored at the end, meaning that a maximum of `namelen-1` characters
// are transferred.
fat_error_t fat_list(const fat_file_t *file, uint32_t index, char *filename, uint8_t namelen);

#ifdef __cplusplus
}
#endif

#endif /*_FAT_H*/
