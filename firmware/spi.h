/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SPI_H
#define _SPI_H

#include <stdbool.h>
#include <stdint.h>

#define SPI_CLK_DIV_2 2
#define SPI_CLK_DIV_4 4
#define SPI_CLK_DIV_8 8
#define SPI_CLK_DIV_16 16
#define SPI_CLK_DIV_32 32
#define SPI_CLK_DIV_64 64
#define SPI_CLK_DIV_128 128

// this can be used for testing clock ranges at compile time
#define SPI_CLOCK(div) (F_CPU / div)
// example:
// #if SPI_CLOCK(SPI_CLK_DIV_64) < 100000 || SPI_CLOCK(SPI_CLK_DIV_64) > 400000
// #warning SPI clock divider is out of range
// #endif

// CPOL=0, CPHA=0
#define SPI_MODE0 0x00
// CPOL=0, CPHA=1
#define SPI_MODE1 0x04
// CPOL=1, CPHA=0
#define SPI_MODE2 0x08
// CPOL=1, CPHA=1
#define SPI_MODE3 0x0c

void spi_init(uint8_t clock, uint8_t mode, bool lsb);
void spi_disable();
uint8_t spi_transfer(uint8_t data);

#endif //_SPI_H
