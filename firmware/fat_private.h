/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FAT_PRIVATE_H
#define _FAT_PRIVATE_H

#include <stdint.h>
#include "fat_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

extern fat_read_block_t *fat_read_func;
extern uint8_t *fat_cached_block;
extern uint32_t fat_cached_blockid;
extern uint32_t fat_boot_blockid;
extern uint32_t fat_partition_size;
extern uint32_t fat_offset;
extern uint32_t fat_length;
extern uint32_t fat_cluster_offset;
extern uint32_t fat_cluster_count;
extern uint32_t fat_cluster_shift;
extern uint32_t fat_cluster_mask;
extern uint32_t fat_root_cluster;
extern uint32_t fat_selected_entries;
extern uint32_t fat_selected_cluster;
extern uint32_t fat_selected_cluster_block;

void fat_read_helper(uint32_t blockid);
fat_error_t fat_parse_mbr();
fat_error_t fat_parse_boot();
fat_error_t fat_find_next_cluster(uint32_t number, uint32_t *clusterid);
fat_error_t fat_load_cluster_block(uint32_t startcluster, uint32_t blocknumber, bool nofatchain);
fat_error_t fat_load_directory_entry(uint32_t startcluster, uint32_t number, fat_dir_entry_t **entry, bool nofatchain);
fat_error_t fat_find_file(const char *name, uint8_t namelen, fat_file_t *data);

#ifdef __cplusplus
}
#endif

#endif /*_FAT_PRIVATE_H*/
