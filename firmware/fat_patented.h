/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FAT_PATENTED_H
#define _FAT_PATENTED_H

#include <stdint.h>
#include "fat.h"

#ifdef __cplusplus
extern "C" {
#endif

fat_error_t fat_verify_boot_checksum();
uint16_t fat_name_hash(const uint16_t *name, uint8_t len);

#ifdef __cplusplus
}
#endif

#endif /*_FAT_PATENTED_H*/
