/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include "fat_patented.h"
#include "fat_private.h"
#include "fat_struct.h"

fat_error_t fat_verify_boot_checksum() {
	uint32_t checksum = 0;
	// the boot sector is 11 blocks long
	for (uint8_t block = 0; block < 11; block++) {
		fat_read_helper(fat_boot_blockid + block);
		if (!fat_cached_block) return FAT_ERROR_READ;
		for (uint16_t index = 0; index < FAT_BLOCK_SIZE; index++) {
			// skip bytes that contain the VolumeFlags and PercentInUse fields
			if (block == 0 && (index == 106 || index == 107 || index == 112)) {
				continue;
			}
			checksum = ((checksum & 0x00000001) ? 0x80000000 : 0) + (checksum >> 1) + fat_cached_block[index];
		}
	}
	// read the checksum block
	fat_read_helper(fat_boot_blockid + FAT_BOOT_CHECKSUM_OFFSET);
	if (!fat_cached_block) return FAT_ERROR_READ;
	uint32_t *checksums = (uint32_t *) fat_cached_block;
	for (uint8_t index = 0; index < (uint8_t) (FAT_BLOCK_SIZE / sizeof(uint32_t)); index++) {
		if (checksum != checksums[index]) {
			return FAT_ERROR_CHECKSUM;
		}
	}
	return FAT_OK;
}

uint16_t fat_name_hash(const uint16_t *name, uint8_t len) {
	uint8_t *buffer = (uint8_t *) name;
	uint16_t bytelen = (uint16_t) len * 2;
	uint16_t hash = 0;
	for (uint16_t index = 0; index < bytelen; index++) {
		hash = ((hash & 1) ? 0x8000 : 0) + (hash >> 1) + (uint16_t) buffer[index];
	}
	return hash;
}
