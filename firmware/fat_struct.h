/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FAT_STRUCT_H
#define _FAT_STRUCT_H

#include <stdint.h>
#include "fat.h"

#define FAT_MAX_DIRECTORY_SIZE (256UL*1024UL*1024UL)

typedef struct {
	// This is a Microsoft GUID, i.e. the first 3 groups are little endian,
	// while the last 2 are big endian.
	uint32_t Data1;
	uint16_t Data2;
	uint16_t Data3;
	uint8_t Data4[2];
	uint8_t Data5[6];
} __attribute__((packed)) fat_guid_t;

typedef struct {
	uint8_t head:8;
	uint8_t sector:6;
	uint8_t cylinderh:2;
	uint8_t cylinderl:8;
} __attribute__((packed)) fat_chs_t;

typedef struct {
	struct {
		uint8_t drive:7;
		uint8_t boot:1;
	} status;
	fat_chs_t first;
	uint8_t type;
	fat_chs_t last;
	uint32_t lbafirst;
	uint32_t sectors;
} __attribute__((packed)) fat_mbr_partition_t;

#define FAT_MBR_BLOCKID 0

#define FAT_MBR_SIG0 0x55
#define FAT_MBR_SIG1 0xaa
#define FAT_MBR_TYPE_EXFAT 0x07

typedef struct {
	uint8_t bootstrap[446];
	fat_mbr_partition_t partition[4];
	uint8_t bootsignature[2];
} __attribute__((packed)) fat_mbr_t;

#define FAT_BOOT_OFFSET 0
#define FAT_EXTENDED_BOOT_OFFSET 1
#define FAT_OEM_PARAMS_OFFSET 9
#define FAT_BOOT_CHECKSUM_OFFSET 11
#define FAT_BACKUP_BOOT_OFFSET 12
#define FAT_BACKUP_EXTENDED_BOOT_OFFSET 13
#define FAT_BACKUP_OEM_PARAMS_OFFSET 21
#define FAT_BACKUP_BOOT_CHECKSUM_OFFSET 23

#define FAT_SUPPORTED_MAJOR_VERSION 1

typedef struct {
	uint8_t JumpBoot[3];
	char FileSystemName[8];
	uint8_t MustBeZero[53];
	uint64_t PartitionOffset;
	uint64_t VolumeLength;
	uint32_t FatOffset;
	uint32_t FatLength;
	uint32_t ClusterHeapOffset;
	uint32_t ClusterCount;
	uint32_t FirstClusterOfRootDirectory;
	uint32_t VolumeSerialNumber;
	struct {
		uint8_t Low;
		uint8_t High;
	} FileSystemRevision;
	struct {
		uint16_t ActiveFat:1;
		uint16_t VolumeDirty:1;
		uint16_t MediaFailure:1;
		uint16_t ClearToZero:1;
		uint16_t Reserved:12;
	} VolumeFlags;
	uint8_t BytesPerSectorShift;
	uint8_t SectorsPerClusterShift;
	uint8_t NumberOfFats;
	uint8_t DriveSelect;
	uint8_t PercentInUse;
	uint8_t Reserved[7];
	uint8_t BootCode[390];
	uint16_t BootSignature;
} __attribute__((packed)) fat_boot_sector_t;

typedef struct {
	fat_guid_t VendorGuid;
	uint8_t CustomDefined[32];
} __attribute__((packed)) fat_boot_generic_parameters_t;

#define FAT_BOOT_NULL_PARAMETERS_GUID { 0x00000000, 0x0000, 0x0000, { 0x00, 0x00 }, { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } }

typedef struct {
	fat_guid_t VendorGuid;
	uint8_t Reserved[32];
} __attribute__((packed)) fat_boot_null_parameters_t;

#define FAT_BOOT_FLASH_PARAMETERS_GUID { 0x0A0C7E46, 0x3399, 0x4021, { 0x90, 0xC8 }, { 0xFA, 0x6D, 0x38, 0x9C, 0x4B, 0xA2 } }

typedef struct {
	fat_guid_t VendorGuid;
	uint32_t EraseBlockSize;
	uint32_t PageSize;
	uint32_t SpareSectors;
	uint32_t RandomAccessTime;
	uint32_t ProgrammingTime;
	uint32_t ReadCycle;
	uint32_t WriteCycle;
	uint8_t Reserved[4];
} __attribute__((packed)) fat_boot_flash_parameters_t;

#define FAT_ENTRY_MEDIA_TYPE_EXFAT 0xfffffff8
#define FAT_ENTRY_LAST 0xffffffff
#define FAT_ENTRY_BAD 0xfffffff7

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t CustomDefined[19];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t SecondaryCount;
	uint16_t SetChecksum;
	struct {
		uint16_t AllocationPossible:1;
		uint16_t NoFatChain:1;
		uint16_t CustomDefined:14;
	} GeneralPrimaryFlags;
	uint8_t CustomDefined[14];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_generic_primary_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t AllocationPossible:1;
		uint8_t NoFatChain:1;
		uint8_t CustomDefined:6;
	} GeneralSecondaryFlags;
	uint8_t CustomDefined[18];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_generic_secondary_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t BitmapIdentifier:1;
		uint8_t Reserved:7;
	} BitmapFlags;
	uint8_t CustomDefined[18];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_allocation_bitmap_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t Reserved1[3];
	uint32_t TableChecksum;
	uint8_t Reserved2[12];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_upcase_table_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t CharacterCount;
	uint16_t VolumeLabel[11];
	uint8_t Reserved[8];
} __attribute__((packed)) fat_volume_label_dir_entry_t;

typedef struct {
	uint32_t DoubleSeconds:5;
	uint32_t Minute:6;
	uint32_t Hour:5;
	uint32_t Day:5;
	uint32_t Month:4;
	uint32_t Year:7;
} fat_timestamp_t;

typedef struct {
	uint8_t OffsetFromUtc:7;
	uint8_t OffsetValid:1;
} fat_utc_offset_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t SecondaryCount;
	uint16_t SetChecksum;
	struct {
		uint16_t ReadOnly:1;
		uint16_t Hidden:1;
		uint16_t System:1;
		uint16_t Reserved1:1;
		uint16_t Directory:1;
		uint16_t Archive:1;
		uint16_t Reserved2:10;
	} FileAttributes;
	uint8_t Reserved1[2];
	fat_timestamp_t CreateTimestamp;
	fat_timestamp_t LastModifiedTimestamp;
	fat_timestamp_t LastAccessedTimestamp;
	uint8_t Create10msIncrement;
	uint8_t LastModified10msIncrement;
	fat_utc_offset_t CreateUtcOffset;
	fat_utc_offset_t LastModifiedUtcOffset;
	fat_utc_offset_t LastAccessedUtcOffset;
	uint8_t Reserved2[7];
} __attribute__((packed)) fat_file_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	uint8_t SecondaryCount;
	uint16_t SetChecksum;
	struct {
		uint16_t AllocationPossible:1;
		uint16_t NoFatChain:1;
		uint16_t CustomDefined:14;
	} GeneralPrimaryFlags;
	fat_guid_t VolumeGuid;
	uint8_t Reserved[10];
} __attribute__((packed)) fat_volume_guid_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t AllocationPossible:1;
		uint8_t NoFatChain:1;
		uint8_t CustomDefined:6;
	} GeneralSecondaryFlags;
	uint8_t Reserved1;
	uint8_t NameLength;
	uint16_t NameHash;
	uint8_t Reserved2[2];
	uint64_t ValidDataLength;
	uint8_t Reserved3[4];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_stream_extension_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t AllocationPossible:1;
		uint8_t NoFatChain:1;
		uint8_t CustomDefined:6;
	} GeneralSecondaryFlags;
	uint16_t FileName[15];
} __attribute__((packed)) fat_file_name_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t AllocationPossible:1;
		uint8_t NoFatChain:1;
		uint8_t CustomDefined:6;
	} GeneralSecondaryFlags;
	fat_guid_t VendorGuid;
	uint8_t VendorDefined[14];
} __attribute__((packed)) fat_vendor_extension_dir_entry_t;

typedef struct {
	struct {
		uint8_t TypeCode:5;
		uint8_t TypeImportance:1;
		uint8_t TypeCategory:1;
		uint8_t InUse:1;
	} EntryType;
	struct {
		uint8_t AllocationPossible:1;
		uint8_t NoFatChain:1;
		uint8_t CustomDefined:6;
	} GeneralSecondaryFlags;
	fat_guid_t VendorGuid;
	uint8_t VendorDefined[2];
	uint32_t FirstCluster;
	uint64_t DataLength;
} __attribute__((packed)) fat_vendor_allocation_dir_entry_t;

#define FAT_DIR_ENTRY_CODE_END 0x00
#define FAT_DIR_ENTRY_CODE_ALLOCATION_BITMAP 0x01
#define FAT_DIR_ENTRY_CODE_UPCASE_TABLE 0x02
#define FAT_DIR_ENTRY_CODE_VOLUME_LABEL 0x03
#define FAT_DIR_ENTRY_CODE_FILE 0x05
#define FAT_DIR_ENTRY_CODE_VOLUME_GUID 0x00
#define FAT_DIR_ENTRY_CODE_TEXFAT_PADDING 0x01
#define FAT_DIR_ENTRY_CODE_STREAM_EXTENSION 0x00
#define FAT_DIR_ENTRY_CODE_FILE_NAME 0x01
#define FAT_DIR_ENTRY_CODE_VENDOR_EXTENSION 0x00
#define FAT_DIR_ENTRY_CODE_VENDOR_ALLOCATION 0x01

// TODO don't hardcode the block/sector size. use BytesPerSectorShift.
#define FAT_DIR_ENTRIES_PER_BLOCK (FAT_BLOCK_SIZE/sizeof(fat_dir_entry_t))

typedef struct {
	fat_dir_entry_t DirectoryEntry[FAT_DIR_ENTRIES_PER_BLOCK];
} __attribute__((packed)) fat_dir_entries_t;

#define FAT_ENTRIES_PER_BLOCK (FAT_BLOCK_SIZE/sizeof(uint32_t))

typedef struct {
	uint32_t FatEntry[FAT_ENTRIES_PER_BLOCK];
} __attribute__((packed)) fat_entries_t;

#define FAT_ALLOCATION_BITMAP_BYTES_PER_BLOCK (FAT_BLOCK_SIZE)
#define FAT_ALLOCATION_BITMAP_ENTRIES_PER_BLOCK (FAT_BLOCK_SIZE*8)

typedef struct {
	uint8_t BitmapBytes[FAT_ALLOCATION_BITMAP_BYTES_PER_BLOCK];
} __attribute__((packed)) fat_allocation_bitmap_t;

#endif /*_FAT_STRUCT_H*/
