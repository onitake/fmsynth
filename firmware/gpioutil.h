/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _GPIOUTIL_H
#define _GPIOUTIL_H

#include <avr/io.h>

#define GPIOU_PA0_BLOCK A
#define GPIOU_PA1_BLOCK A
#define GPIOU_PB0_BLOCK B
#define GPIOU_PB1_BLOCK B
#define GPIOU_PB2_BLOCK B
#define GPIOU_PB3_BLOCK B
#define GPIOU_PB4_BLOCK B
#define GPIOU_PB5_BLOCK B
#define GPIOU_PB6_BLOCK B
#define GPIOU_PB7_BLOCK B
#define GPIOU_PC0_BLOCK C
#define GPIOU_PC1_BLOCK C
#define GPIOU_PC2_BLOCK C
#define GPIOU_PC3_BLOCK C
#define GPIOU_PC4_BLOCK C
#define GPIOU_PC5_BLOCK C
#define GPIOU_PC6_BLOCK C
#define GPIOU_PC7_BLOCK C
#define GPIOU_PD0_BLOCK D
#define GPIOU_PD1_BLOCK D
#define GPIOU_PD2_BLOCK D
#define GPIOU_PD3_BLOCK D
#define GPIOU_PD4_BLOCK D
#define GPIOU_PD5_BLOCK D
#define GPIOU_PD6_BLOCK D
#define GPIOU_PD7_BLOCK D
#define GPIOU_PA0_BIT 0
#define GPIOU_PA1_BIT 1
#define GPIOU_PB0_BIT 0
#define GPIOU_PB1_BIT 1
#define GPIOU_PB2_BIT 2
#define GPIOU_PB3_BIT 3
#define GPIOU_PB4_BIT 4
#define GPIOU_PB5_BIT 5
#define GPIOU_PB6_BIT 6
#define GPIOU_PB7_BIT 7
#define GPIOU_PC0_BIT 0
#define GPIOU_PC1_BIT 1
#define GPIOU_PC2_BIT 2
#define GPIOU_PC3_BIT 3
#define GPIOU_PC4_BIT 4
#define GPIOU_PC5_BIT 5
#define GPIOU_PC6_BIT 6
#define GPIOU_PC7_BIT 7
#define GPIOU_PD0_BIT 0
#define GPIOU_PD1_BIT 1
#define GPIOU_PD2_BIT 2
#define GPIOU_PD3_BIT 3
#define GPIOU_PD4_BIT 4
#define GPIOU_PD5_BIT 5
#define GPIOU_PD6_BIT 6
#define GPIOU_PD7_BIT 7

#define GPIOU_UNO_D0 GPIOU_PD0
#define GPIOU_UNO_D1 GPIOU_PD1
#define GPIOU_UNO_D2 GPIOU_PD2
#define GPIOU_UNO_D3 GPIOU_PD3
#define GPIOU_UNO_D4 GPIOU_PD4
#define GPIOU_UNO_D5 GPIOU_PD5
#define GPIOU_UNO_D6 GPIOU_PD6
#define GPIOU_UNO_D7 GPIOU_PD7
#define GPIOU_UNO_D8 GPIOU_PB0
#define GPIOU_UNO_D9 GPIOU_PB1
#define GPIOU_UNO_D10 GPIOU_PB2
#define GPIOU_UNO_D11 GPIOU_PB3
#define GPIOU_UNO_D12 GPIOU_PB4
#define GPIOU_UNO_D13 GPIOU_PB5
#define GPIOU_UNO_A0 GPIOU_PC0
#define GPIOU_UNO_A1 GPIOU_PC1
#define GPIOU_UNO_A2 GPIOU_PC2
#define GPIOU_UNO_A3 GPIOU_PC3
#define GPIOU_UNO_A4 GPIOU_PC4
#define GPIOU_UNO_A5 GPIOU_PC5

#define GPIOU_M8_MOSI GPIOU_PB3
#define GPIOU_M8_MISO GPIOU_PB4
#define GPIOU_M8_SCK GPIOU_PB5

#define GPIOU_BLOCK_PASTE(pd) pd##_BLOCK
#define GPIOU_BLOCK(pd) GPIOU_BLOCK_PASTE(pd)
#define GPIOU_BIT_PASTE(pd) pd##_BIT
#define GPIOU_BIT(pd) GPIOU_BIT_PASTE(pd)

#define GPIOU_REG_PASTE(reg, block) reg##block
#define GPIOU_REG(reg, block) GPIOU_REG_PASTE(reg, block)

#define GPIOU_BIT_MASK(bit) (1 << bit)
#define GPIOU_BIT_SET(reg, bit) reg |= GPIOU_BIT_MASK(bit)
#define GPIOU_BIT_CLEAR(reg, bit) reg &= ~GPIOU_BIT_MASK(bit)
#define GPIOU_BIT_GET(reg, bit) (reg & GPIOU_BIT_MASK(bit))

#define GPIOU_SET(reg, pd) GPIOU_BIT_SET(GPIOU_REG(reg, GPIOU_BLOCK(pd)), GPIOU_BIT(pd))
#define GPIOU_CLEAR(reg, pd) GPIOU_BIT_CLEAR(GPIOU_REG(reg, GPIOU_BLOCK(pd)), GPIOU_BIT(pd))
#define GPIOU_GET(reg, pd) GPIOU_BIT_GET(GPIOU_REG(reg, GPIOU_BLOCK(pd)), GPIOU_BIT(pd))

#define GPIOU_IN_HIZ(pd) do {\
	GPIOU_CLEAR(DDR, pd);\
	GPIOU_CLEAR(PORT, pd);\
} while (0)
#define GPIOU_IN_PUP(pd) do {\
	GPIOU_CLEAR(DDR, pd);\
	GPIOU_SET(PORT, pd);\
} while (0)
#define GPIOU_OUT_LOW(pd) do {\
	GPIOU_CLEAR(PORT, pd);\
	GPIOU_SET(DDR, pd);\
} while (0)
#define GPIOU_OUT_HIGH(pd) do {\
	GPIOU_SET(PORT, pd);\
	GPIOU_SET(DDR, pd);\
} while (0)
#define GPIOU_NOP() do {} while (0)

#endif //_GPIOUTIL_H
