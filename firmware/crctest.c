/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "crc.h"

void printb(unsigned int number, int num_digits) {
    for (int digit = num_digits - 1; digit >= 0; digit--) {
        putchar(number & (1 << digit) ? '1' : '0');
    }
}

void test_crc7() {
	uint8_t test1[] = { 0b01000000, 0b00000000, 0b00000000, 0b00000000, 0b00000000, };
	uint8_t crc1 = sd_crc7(test1, sizeof(test1));
	printf("crc1=0b"); printb(crc1, 8); printf("\nexp=0b"); printb(0b1001010, 8); printf("\n");
	assert(crc1 == 0b1001010);
	uint8_t test2[] = { 0b01010001, 0b00000000, 0b00000000, 0b00000000, 0b00000000, };
	uint8_t crc2 = sd_crc7(test2, sizeof(test2));
	printf("crc=0b"); printb(crc2, 8); printf("\nexp=0b"); printb(0b0101010, 8); printf("\n");
	assert(crc2 == 0b0101010);
	uint8_t test3[] = { 0b00010001, 0b00000000, 0b00000000, 0b00001001, 0b00000000, };
	uint8_t crc3 = sd_crc7(test3, sizeof(test3));
	printf("crc=0b"); printb(crc3, 8); printf("\nexp=0b"); printb(0b0110011, 8); printf("\n");
	assert(crc3 == 0b0110011);
}

void test_crc16() {
	uint8_t test[512];
	memset(test, 0xff, sizeof(test));
	uint16_t crc = sd_crc16(test, sizeof(test));
	printf("crc=0b"); printb(crc, 16); printf("\nexp=0b"); printb(0x7FA1, 16); printf("\n");
	assert(crc == 0x7FA1);
}

typedef struct {
	// bit sequence on wire:
	// sdcccccc aaaaaaaa aaaaaaaa aaaaaaaa aaaaaaaa rrrrrrrt
	uint8_t data[6];
} __attribute__((__packed__)) sd_cmd_t;

void sd_send_cmd(sd_cmd_t *command) {
	// set start (0) and direction bits (1)
	command->data[0] = (command->data[0] & ~0xc0) | 0x40;
	// calculate crc and append stop bit
	command->data[5] = (sd_crc7(command->data, 5) << 1) | 0x01;
	for (uint8_t i = 0; i < sizeof(command->data); i++) {
		printb(command->data[i], 8); printf(" ");
	}
	printf("\n");
}

void test_sdcmd() {
	sd_cmd_t test1 = { { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 } };
	sd_send_cmd(&test1);
	sd_cmd_t test2 = { { 0x11, 0x00, 0x00, 0x00, 0x00, 0x00 } };
	sd_send_cmd(&test2);
}

int main() {
	test_crc7();
	test_crc16();
	test_sdcmd();
}
