/*
 * Copyright (C) 2021 Gregor Riepl <onitake@gmail.com>
 *
 * Based on:
 *
 * Adplug - Replayer for many OPL2/OPL3 audio file formats.
 * Copyright (C) 1999 - 2004 Simon Peter, <dn.tlp@gmx.net>, et al.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * hsc.cpp - HSC Player by Simon Peter <dn.tlp@gmx.net>
 */

#include <avr/pgmspace.h>
#include "hsc.h"
#include "ymf.h"

#define ARRAY_LEN(x) (sizeof(x) / sizeof(x[0]))
#define NUM_CHANNELS 9
#define NUM_ORDERS 50
#define NUM_NOTES 64

// TODO declare song data extern and compile song file into separate object
// include your song here
#include "songs/victory.h"
//#include "songs/neo.h"
//#include "songs/gameover.h"

// these must be defined in the song include file
//const PROGMEM uint8_t instruments[][12];
//const PROGMEM uint8_t song[];
//const PROGMEM uint8_t patterns[][NUM_NOTES*NUM_CHANNELS*2];

// the twelve base tones of the diatonic scale, starting with C#
// the block register sets the octave
const PROGMEM uint16_t note_table_old[12] = {
	363, 385, 408, 432, 458, 485, 514, 544, 577, 611, 647, 686,
};
const PROGMEM uint16_t note_table[12] = {
	//0x157, //	261.7	C
	0x16B, //	277.2	C#
	0x181, //	293.7	D
	0x198, //	311.1	D#
	0x1B0, //	329.6	E
	0x1CA, //	349.2	F
	0x1E5, //	370.0	F#
	0x202, //	392.0	G
	0x220, //	415.3	G#
	0x241, //	440.0	A
	0x263, //	466.2	A#
	0x287, //	493.9	B
	0x2AE, //	523.3	C
};

const PROGMEM uint8_t op_table[NUM_CHANNELS] = {
	//0x00, 0x01, 0x02, 0x08, 0x09, 0x0a, 0x10, 0x11, 0x12,
	0, 1, 2, 8, 9, 10, 16, 17, 18,
	// shouldn't the operators be these instead?
	//0, 1, 2, 6, 7, 8, 12, 13, 14,
	// the drums use 13, 14, 16, 17, 18
};

// which instrument is currently mapped to each channel
static uint8_t channel_instrument[NUM_CHANNELS];
// are we currently playing a sliding note?
static uint8_t channel_slide[NUM_CHANNELS];
// current base frequency for this channel
static uint8_t channel_frequency[NUM_CHANNELS];
// cached ADL register (KON|BLOCK|FNUM(H) on the OPL3)
static uint8_t adl_frequency[NUM_CHANNELS];
// player status
static uint8_t pattpos, songpos, songend, pattbreak, mode6, bd, fadein;
static uint32_t speed, del;

static void set_frequency(uint8_t channel, uint16_t frequency) {
	adl_frequency[channel] = (adl_frequency[channel] & ~3) | (frequency >> 8);

	ymf_write_reg(0, 0xa0 + channel, frequency & 0xff);
	ymf_write_reg(0, 0xb0 + channel, adl_frequency[channel]);
}

static void set_volume(uint8_t channel, uint8_t volc, uint8_t volm) {
	// calculate the instrument base pointer for the instrument selected on this channel
	const uint8_t *instr = instruments[channel_instrument[channel]];
	// get the channel-to-operator mapping
	uint8_t op = pgm_read_byte(&op_table[channel]);

	// read and de-mangle the volume settings from the instrument
	uint8_t volc_instr = pgm_read_byte(&instr[2]);
	volc_instr ^= (volc_instr & 0x40) << 1;
	uint8_t volm_instr = pgm_read_byte(&instr[3]);
	volm_instr ^= (volm_instr & 0x40) << 1;

	// combine with the desired volumes and set them

	// carrier
	ymf_write_reg(0, 0x43 + op, volc | (volc_instr & ~63));
	// modulator
	// no idea what this flag is - does it select if we have two carriers vs. one carrier + one modulator? AM vs. FM?
	if (pgm_read_byte(&instr[8]) & 1) {
		ymf_write_reg(0, 0x40 + op, volm | (volm_instr & ~63));
	} else {
		ymf_write_reg(0, 0x40 + op, volm_instr);
	}
}

void set_instrument(uint8_t channel, uint8_t number) {
	if (channel > NUM_CHANNELS || number >= ARRAY_LEN(instruments)) {
		// out of range, do nothing
		return;
	}

	// calculate the instrument base pointer
	const uint8_t *instr = instruments[number];
	// get the channel-to-operator mapping
	uint8_t op = pgm_read_byte(&op_table[channel]);

	// save the instrument assignment
	channel_instrument[channel] = number;

	// stop old note
	ymf_write_reg(0, 0xb0 + channel, 0);

	// set instrument
	ymf_write_reg(0, 0xc0 + channel, pgm_read_byte(&instr[8]));
	ymf_write_reg(0, 0x23 + op, pgm_read_byte(&instr[0]));        // carrier
	ymf_write_reg(0, 0x20 + op, pgm_read_byte(&instr[1]));        // modulator
	ymf_write_reg(0, 0x63 + op, pgm_read_byte(&instr[4]));        // bits 0..3 = decay; 4..7 = attack
	ymf_write_reg(0, 0x60 + op, pgm_read_byte(&instr[5]));
	ymf_write_reg(0, 0x83 + op, pgm_read_byte(&instr[6]));        // 0..3 = release; 4..7 = sustain
	ymf_write_reg(0, 0x80 + op, pgm_read_byte(&instr[7]));
	ymf_write_reg(0, 0xe3 + op, pgm_read_byte(&instr[9]));        // bits 0..1 = wave form
	ymf_write_reg(0, 0xe0 + op, pgm_read_byte(&instr[10]));

	// the top bits will be read again in set_volume, so we don't need to demangle them here
	set_volume(channel, pgm_read_byte(&instr[2]) & 63, pgm_read_byte(&instr[3]) & 63);
}

void song_init() {
	pattpos = 0; songpos = 0; pattbreak = 0; speed = 2;
	del = 1; songend = 0; mode6 = 0; bd = 0; fadein = 0;

	ymf_write_reg(0, 1, 32);
	ymf_write_reg(0, 8, 128);
	ymf_write_reg(0, 0xbd, 0);
	for(uint8_t i = 0; i < ARRAY_LEN(instruments); i++) {
		// assign initial instruments: channel x = instrument x
		set_instrument(i, i);
	}
}

void note_play(uint8_t channel, uint8_t volc, uint8_t volm, uint16_t frequency) {
	set_volume(channel, volc, volm);
	set_frequency(channel, frequency);
	adl_frequency[channel] |= 0x20;
	ymf_write_reg(0, 0xb0 + channel, adl_frequency[channel]);
}

void note_stop(uint8_t channel) {
	adl_frequency[channel] &= ~0x20;
	ymf_write_reg(0, 0xb0 + channel, adl_frequency[channel]);
}

bool song_update() {
	// general vars
	uint8_t chan, pattnr, note, effect, eff_op, inst, vol, Okt, db, voltmp;
	uint16_t Fnr;
	uint32_t pattoff;

	// player speed handling
	del--;
	if (del) {
		// nothing done
		return !songend;
	}

	// fade-in handling
	if (fadein) {
		fadein--;
	}

	// verify we're not past the end of the song
	if (songpos >= ARRAY_LEN(song)) {
		songend = 1;
		return !songend;
	}

	pattnr = pgm_read_byte(&song[songpos]);
	// 0xff indicates song end, but this prevents a crash for some songs that
	// use other weird values, like 0xbf
	if (pattnr >= 0xb2) {			// arrangement handling
		songend = 1;				// set end-flag
		songpos = 0;
		// verify we're not past the end of the song now
		if (songpos >= ARRAY_LEN(song)) {
			return !songend;
		}
		pattnr = pgm_read_byte(&song[songpos]);
	} else {
		if ((pattnr & 128) && (pattnr <= 0xb1)) { // goto pattern "nr"
			songend = 1;
			songpos = pattnr & 127;
			// verify we're not past the end of the song now
			if (songpos >= ARRAY_LEN(song)) {
				return !songend;
			}
			pattnr = pgm_read_byte(&song[songpos]);
			pattpos = 0;
		}
	}

	pattoff = pattpos*9;
	for (chan=0;chan<NUM_CHANNELS;chan++) {			// handle all channels
		note = pgm_read_byte(&patterns[pattnr][pattoff*2]);
		effect = pgm_read_byte(&patterns[pattnr][pattoff*2+1]);
		pattoff++;

		if(note & 128) {                    // set instrument
			set_instrument(chan,effect);
			continue;
		}
		eff_op = effect & 0x0f;
		inst = channel_instrument[chan];
		if(note) {
			channel_slide[chan] = 0;
		}

		switch (effect & 0xf0) {			// effect handling
		case 0:								// global effect
			/* The following fx are unimplemented on purpose:
			* 02 - Slide Mainvolume up
			* 03 - Slide Mainvolume down (here: fade in)
			* 04 - Set Mainvolume to 0
			*
			* This is because i've never seen any HSC modules using the fx this way.
			* All modules use the fx the way, i've implemented it.
			*/
			switch(eff_op) {
				case 1: pattbreak++; break;	// jump to next pattern
				case 3: fadein = 31; break;	// fade in (divided by 2)
				case 5: mode6 = 1; break;	// 6 voice mode on
				case 6: mode6 = 0; break;	// 6 voice mode off
			}
			break;
		case 0x20:
		case 0x10:		                    // manual slides
			if (effect & 0x10) {
				channel_frequency[chan] += eff_op;
				channel_slide[chan] += eff_op;
			} else {
				channel_frequency[chan] -= eff_op;
				channel_slide[chan] -= eff_op;
			}
			if(!note) {
				set_frequency(chan,channel_frequency[chan]);
			}
			break;
		case 0x50:							// set percussion instrument (unimplemented)
			break;
		case 0x60:							// set feedback
			ymf_write_reg(0, 0xc0 + chan, (pgm_read_byte(&instruments[channel_instrument[chan]][8]) & 1) + (eff_op << 1));
			break;
		case 0xa0:		                    // set carrier volume
			vol = eff_op << 2;
			voltmp = pgm_read_byte(&instruments[channel_instrument[chan]][2]);
			voltmp ^= (voltmp & 0x40) << 1;
			ymf_write_reg(0, 0x43 + pgm_read_byte(&op_table[chan]), vol | (voltmp & ~63));
			break;
		case 0xb0:		                    // set modulator volume
			vol = eff_op << 2;
			voltmp = pgm_read_byte(&instruments[channel_instrument[chan]][3]);
			voltmp ^= (voltmp & 0x40) << 1;
			if (pgm_read_byte(&instruments[inst][8]) & 1) {
				voltmp = pgm_read_byte(&instruments[channel_instrument[chan]][3]);
				voltmp ^= (voltmp & 0x40) << 1;
				ymf_write_reg(0, 0x40 + op_table[chan], vol | (voltmp & ~63));
			} else {
				voltmp = pgm_read_byte(&instruments[inst][3]);
				voltmp ^= (voltmp & 0x40) << 1;
				ymf_write_reg(0, 0x40 + op_table[chan],vol | (voltmp & ~63));
			}
			break;
		case 0xc0:		                    // set instrument volume
			db = eff_op << 2;
			voltmp = pgm_read_byte(&instruments[channel_instrument[chan]][2]);
			voltmp ^= (voltmp & 0x40) << 1;
			ymf_write_reg(0, 0x43 + op_table[chan], db | (voltmp & ~63));
			if (pgm_read_byte(&instruments[inst][8]) & 1) {
				voltmp = pgm_read_byte(&instruments[channel_instrument[chan]][3]);
				voltmp ^= (voltmp & 0x40) << 1;
				ymf_write_reg(0, 0x40 + op_table[chan], db | (voltmp & ~63));
			}
			break;
		case 0xd0:
			pattbreak++; songpos = eff_op; songend = 1;	// position jump
			break;
		case 0xf0:							// set speed
			speed = eff_op;
			del = ++speed;
			break;
		}

		if(fadein) {						// fade-in volume setting
			set_volume(chan,fadein*2,fadein*2);
		}

		if(!note) {						// note handling
			continue;
		}
		note--;

		if ((note == 0x7f-1) || ((note/12) & ~7)) {    // pause (7fh)
			adl_frequency[chan] &= ~32;
			ymf_write_reg(0, 0xb0 + chan,adl_frequency[chan]);
			continue;
		}

		// play the note
		Okt = ((note/12) & 7) << 2;
		Fnr = pgm_read_word(&note_table[(note % 12)]) + pgm_read_byte(&instruments[inst][11]) + channel_slide[chan];
		channel_frequency[chan] = Fnr;
		if(!mode6 || chan < 6) {
			adl_frequency[chan] = Okt | 32;
		} else {
			adl_frequency[chan] = Okt;		// never set key for drums
		}
		ymf_write_reg(0, 0xb0 + chan, 0);
		set_frequency(chan,Fnr);
		if(mode6) {
			switch(chan) {		// play drums
			case 6: ymf_write_reg(0, 0xbd,bd & ~16); bd |= 48; break;	// bass drum
			case 7: ymf_write_reg(0, 0xbd,bd & ~1); bd |= 33; break;	// hihat
			case 8: ymf_write_reg(0, 0xbd,bd & ~2); bd |= 34; break;	// cymbal
			}
			ymf_write_reg(0, 0xbd,bd);
		}
	}

	del = speed;		// player speed-timing
	if(pattbreak) {		// do post-effect handling
		pattpos=0;			// pattern break!
		pattbreak=0;
		songpos++;
		songpos %= 50;
		if(!songpos)
		songend = 1;
	} else {
		pattpos++;
		pattpos &= 63;		// advance in pattern data
		if (!pattpos) {
		songpos++;
		songpos %= 50;
		if(!songpos)
		songend = 1;
		}
	}
	return !songend;		// still playing
}
