/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "crc.h"

uint8_t sd_crc7_update(uint8_t crc, uint8_t data) {
	// G(x) = x^7 + x^3 + 1
	// G  = 1000 1001 & 0x7f = 0x09 (forward)
	// G' =  100 1000        = 0x48 (reverse)
	// only rotate the significant bits, not x^7)
#if 0
	// reverse version
	crc ^= data;
	for (uint8_t i = 0; i < 8; i++) {
		if (crc & 1) {
			crc = (crc >> 1) ^ 0x48;
		} else {
			crc >>= 1;
		}
	}
#else
	// forward implementation
	for (uint8_t ibit = 0; ibit < 8; ibit++) {
		crc <<= 1;
		if ((data ^ crc) & 0x80) {
			crc ^= 0x09;
		}
		data <<= 1;
	}
#endif
	return crc;
}

uint16_t sd_crc16_update(uint16_t crc, uint8_t data) {
	// G(x) = x^16 + x^12 + x^5 + 1
	// G  = 1 0001 0000 0010 0001 & 0xffff = 0x1021 (forward)
	// G' =   1000 0100 0000 1000          = 0x8408 (reverse)
	// only rotate the significant bits, not x^16)
#if 0
	// reverse version
	crc ^= data;
	for (uint8_t i = 0; i < 8; i++) {
		if (crc & 1) {
			crc = (crc >> 1) ^ 0x8408;
		} else {
			crc >>= 1;
		}
	}
#elif 0
	// reverse version, optimized
	data ^= (uint8_t) (crc & 0xff);
	data ^= data << 4;
	return ((((uint16_t) data << 8) | ((crc >> 8) & 0xff)) ^ (uint8_t) (data >> 4) ^ ((uint16_t) data << 3));
#else
	// forward version
	crc ^= (uint16_t) data << 8;
	for (uint8_t i = 0; i < 8; i++) {
		if (crc & 0x8000) {
			crc = (crc << 1) ^ 0x1021;
		} else {
			crc <<= 1;
		}
	}
#endif
	return crc;
}

uint16_t sd_crc7(uint8_t *data, uint8_t length) {
	uint8_t crc = 0;
	for (uint8_t i = 0; i < length; i++) {
		crc = sd_crc7_update(crc, data[i]);
	}
	// this is only a 7-bit CRC, mask the top bit
	return crc & 0x7f;
}

uint16_t sd_crc16(uint8_t *data, uint16_t length) {
	uint16_t crc = 0;
	for (uint16_t i = 0; i < length; i++) {
		crc = sd_crc16_update(crc, data[i]);
	}
	return crc;
}
