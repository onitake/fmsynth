/*
 *  Copyright © 2023 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "uart.h"
#include "spi.h"
#include "gpio.h"
#include "sd.h"
#include "fat.h"
#include "ymf.h"
#include "clock.h"
#include "rdos.h"
#include "vgm.h"

// static buffer for block reads
__attribute__((section (".noinit")))
static struct {
	uint8_t buffer[512];
	char filename[512];
	uint64_t offset;
	fat_file_t *playedfile;
	bool ready;
} state;

uint8_t *buffered_read(uint32_t blockid) {
	uint8_t status = sd_read(blockid, state.buffer);
	if (status != SD_GENERAL_OK) {
		printf_P(PSTR("error reading block 0x%08x\n"), blockid);
		return NULL;
	}
	return state.buffer;
}

static uint32_t getnum() {
	uint32_t ret = 0;
	for (uint8_t index = 0; index < 10;) {
		int c = getchar();
		if (c >= '0' && c <= '9') {
			uint8_t digit = c - '0';
			if (ret <= (UINT32_MAX - digit) / 10) {
				putchar(c);
				ret = ret * 10 + digit;
				index++;
			}
		} else if (c == '\n') {
			putchar(c);
			return ret;
		}
	}
	// eat remaining chars until newline
	while (getchar() != '\n');
	return ret;
}

static uint32_t read_u32(uint64_t ofs) {
	if (ofs + 3 >= state.playedfile->file_size) {
		return 0xffffffff;
	}
	uint8_t ret[4] = { 0, 0, 0, 0 };
	fat_error_t err = fat_read(state.playedfile, ofs, sizeof(ret), ret);
	if (err != FAT_OK) {
		return 0xffffffff;
	}
	return (uint32_t) ret[0] | ((uint32_t) ret[1] << 8) | ((uint32_t) ret[2] << 16) | ((uint32_t) ret[3] << 24);
}

static uint16_t read_u16(uint64_t ofs) {
	if (ofs + 1 >= state.playedfile->file_size) {
		return 0xffff;
	}
	uint8_t ret[2] = { 0, 0 };
	fat_error_t err = fat_read(state.playedfile, ofs, 2, ret);
	if (err != FAT_OK) {
		return 0xffff;
	}
	return (uint16_t) ret[0] | ((uint16_t) ret[1] << 8);
}

static uint8_t read_u8(uint64_t ofs) {
	if (ofs >= state.playedfile->file_size) {
		return 0xff;
	}
	uint8_t ret = 0;
	fat_error_t err = fat_read(state.playedfile, ofs, 1, &ret);
	if (err != FAT_OK) {
		return 0xff;
	}
	return ret;
}

static bool read_bytes(uint8_t *buf, uint8_t len) {
	//printf_P(PSTR("fp=%p ofs=%lu buf=%p len=%u\n"), state.playedfile, (uint32_t) state.offset, buf, len);
	if (state.playedfile->file_size >= len && state.offset >= state.playedfile->file_size - len) {
		return false;
	}
	fat_error_t err = FAT_OK;
	if (buf) {
		err = fat_read(state.playedfile, state.offset, len, buf);
	}
	state.offset += len;
	return err == FAT_OK;
}

static void play_rdos(fat_file_t *fp) {
	state.playedfile = fp;
	state.offset = 0;

	if (!rdos_init(read_u16)) {
		puts_P(PSTR("Invalid file, is this really a RAW OPL dump?"));
		return;
	}

	printf_P(PSTR("Playing song (speed=%u Hz)..."), rdos_speed_hz());

	ymf_begin();
	bool running = true;
	while (running) {
		//ymf_begin();
		running = rdos_tick();
		//ymf_end();
	}
	ymf_end();

	puts_P(PSTR("done."));
}

static void play_vgm(fat_file_t *fp) {
	state.playedfile = fp;
	state.offset = 0;

	vgm_header_t hdr;
	vgm_err_t err = vgm_open(&hdr, read_bytes);
	if (err != VGM_OK) {
		printf_P(PSTR("Can't load file, is this really an uncompressed VGM? Error=%u"), err);
		return;
	}

	printf_P(PSTR("version=%u.%u%u\n"), (hdr.version >> 8) & 0xf, (hdr.version >> 4) & 0xf, (hdr.version >> 0) & 0xf);
	//printf_P(PSTR("data_length=%u\n"), hdr.data_length);
	//printf_P(PSTR("samples=%u\n"), hdr.samples);
	//printf_P(PSTR("loop_offset=%u\n"), hdr.loop_offset);
	//printf_P(PSTR("loop_samples=%u\n"), hdr.loop_samples);
	//printf_P(PSTR("data_offset=%u\n"), hdr.data_offset);
	//printf_P(PSTR("opl_clock=%u\n"), hdr.opl_clock);
	printf_P(PSTR("opl_mode=%u\n"), hdr.opl_mode);
	//printf_P(PSTR("volume_modifier=%u\n"), hdr.volume_modifier);
	//printf_P(PSTR("loop_base=%u\n"), hdr.loop_base);
	//printf_P(PSTR("loop_modifier=%u\n"), hdr.loop_modifier);
	printf_P(PSTR("Song length: %us\n"), hdr.samples / VGM_SAMPLE_RATE);
	if (hdr.opl_mode == VGM_MODE_OPL4 || hdr.opl_mode == VGM_MODE_OPL_MSX) {
		puts_P(PSTR("Unsupported OPL chip, but trying to play anyway."));
	}
	if (hdr.opl_clock != F_OPL) {
		printf_P(PSTR("OPL clock is not the expected %uHz. Got: %uHz. Song pitch may be out of tune.\n"), F_OPL, hdr.opl_clock);
	}
	if (hdr.loop_offset || hdr.loop_samples || hdr.loop_base || hdr.loop_modifier) {
		puts_P(PSTR("Loops aren't supported."));
	}
	if (hdr.volume_modifier) {
		puts_P(PSTR("Volume modifiers aren't supported."));
	}

	puts_P(PSTR("START"));
	bool running = true;
	ymf_begin();
	while (running) {
		vgm_err_t err = vgm_next(&hdr);
		if (err != VGM_OK) {
			printf_P(PSTR("Playback end: %u\n"), err);
			running = false;
		}
	}
	ymf_end();
	puts_P(PSTR("END\n"));
}

static void init() {
	state.ready = false;

	uart_init();
	gpio_init();
	clock_init();

	memset(state.buffer, 0, sizeof(state.buffer));
	memset(state.filename, 0, sizeof(state.filename));
	state.filename[0] = '/';

	puts_P(PSTR("\n"));

	puts_P(PSTR("Detect YMF..."));
	ymf_begin();
	switch (ymf_detect()) {
		case YMF_DETECT_NONE:
			puts_P(PSTR("none found\n"));
			break;
		case YMF_DETECT_OPL2:
			puts_P(PSTR("OPL2 (YM-3812) found\n"));
			break;
		case YMF_DETECT_OPL3:
			puts_P(PSTR("OPL3 (YMF-262) found\n"));
			break;
		default:
			puts_P(PSTR("invalid type\n"));
			break;
	}
	ymf_end();

	puts_P(PSTR("Init SD card..."));
	sd_init();
	if (!sd_inserted()) {
		puts_P(PSTR("warning: insert switch not detected."));
	}
	uint8_t type = sd_start();
	sd_deselect();

	if (type != SD_GENERAL_ERROR) {
		puts_P(PSTR("done. Card is: "));
		switch (type) {
			case SD_CARD_TYPE_V1:
				puts_P(PSTR("V1\n"));
				break;
			case SD_CARD_TYPE_V2:
				puts_P(PSTR("V2+\n"));
				break;
			case SD_CARD_TYPE_V2_HCS:
				puts_P(PSTR("V2+ high capacity (HCS)\n"));
				break;
			default:
				puts_P(PSTR("unknown\n"));
				break;
		}

		puts_P(PSTR("Init exFAT..."));
		sd_select();
		fat_error_t err = fat_init(buffered_read);
		sd_deselect();

		if (err != FAT_OK) {
			printf_P(PSTR("error %u\n"), err);
		} else {
			puts_P(PSTR("done.\n"));
			state.ready = true;
		}
	} else {
		puts_P(PSTR("error initializing card.\n"));
	}
}

static void navigate() {
	printf_P(PSTR("%s\n"), state.filename);

	fat_file_t fp;
	fat_error_t err = fat_open(state.filename, &fp);
	if (err != FAT_OK) {
		printf_P(PSTR("error %u\n"), err);
		state.ready = false;
		return;
	}

	if (fp.flags.directory) {
		printf_P(PSTR("Directory entries: %lu\n"), (unsigned long) fp.file_size);
		printf_P(PSTR("0: ..\n"));

		char subfile[128];
		for (uint32_t i = 0; i < fp.file_size; i++) {
			err = fat_list(&fp, i, subfile, sizeof(subfile));
			if (err != FAT_OK) {
				state.ready = false;
				return;
			}
			printf_P(PSTR("%lu: %s\n"), i + 1, subfile);
		}

		printf_P(PSTR("Choose entry ([0-%lu]): "), (unsigned long) (fp.file_size));
		uint32_t response = getnum();
		if (response > fp.file_size) {
			puts_P(PSTR("Invalid reponse."));
			return;
		}

		if (response == 0) {
			// level up
			puts_P(PSTR("Moving up."));
			char *slash = strrchr(state.filename, '/');
			if (slash != state.filename) {
				// can't move further up than the root
				*slash = 0;
			}
		} else {
			err = fat_list(&fp, response - 1, subfile, sizeof(subfile));
			if (err != FAT_OK) {
				state.ready = false;
				return;
			}
			strncat(state.filename, "/", sizeof(state.filename) - strlen(state.filename));
			strncat(state.filename, subfile, sizeof(state.filename) - strlen(state.filename));
		}

	} else {
		// load file
		/*char str[64];
		for (uint64_t ofs = 0; ofs < fp.file_size;) {
			uint64_t len = sizeof(str) - 1;
			if (ofs + len > fp.file_size) {
				len = fp.file_size - ofs;
			}
			err = fat_read(&fp, ofs, len, str);
			if (err != FAT_OK) {
				printf_P(PSTR("error %u\n"), err);
				ready = false;
				break;
			}
			//fwrite(str, 1, len, &uart_io);
			ofs += len;
		}*/

		// play song
		//play_rdos(&fp);
		play_vgm(&fp);

		// move up
		puts_P(PSTR("Done."));
		char *slash = strrchr(state.filename, '/');
		if (slash != state.filename) {
			*slash = 0;
		}
	}
}

static void loop() {
	if (state.ready) {
		sd_select();
		navigate();
		sd_deselect();
	}
}

int main() {
	init();
	while (1) {
		loop();
		//_delay_ms(1000);
	}
}
