/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>
#include "fat.h"

FILE *fp = NULL;
uint8_t buffer[FAT_BLOCK_SIZE];

uint8_t *read_block(uint32_t blockid) {
	if (fseek(fp, (long) blockid * FAT_BLOCK_SIZE, SEEK_SET) == -1) {
		perror("Can't seek to block");
		exit(1);
	}
	if (fread(buffer, FAT_BLOCK_SIZE, 1, fp) == -1) {
		perror("Can't read block");
		exit(1);
	}
	return buffer;
}

int main(int argc, char **argv) {
	fat_error_t err = FAT_OK;

	if (argc < 2) {
		fprintf(stderr, "Usage: fattest <disk.img>\n");
		return 1;
	}
	fp = fopen(argv[1], "r");
	if (fp == NULL) {
		perror("Can't open disk image");
		return 1;
	}
	err = fat_init(read_block);
	if (err != FAT_OK) {
		fprintf(stderr, "Error loading FAT: %u\n", err);
		return 2;
	}

	char filename[512];
	strncpy(filename, "/", sizeof(filename));
	fat_file_t file = {};

	int running = 1;
	while (running) {
		printf("Opening %s\n", filename);
		err = fat_open(filename, &file);
		if (err != FAT_OK) {
			fprintf(stderr, "Error opening %s: %u\n", filename, err);
			return 2;
		}

		if (file.flags.directory) {
			printf("Found directory \"%s\" at cluster %u, containing %" PRIu64 " entries\n", filename, file.start_cluster, file.file_size);

			char subfile[255];
			for (uint32_t i = 0; i < file.file_size; i++) {
				err = fat_list(&file, i, subfile, sizeof(subfile));
				if (err != FAT_OK) {
					fprintf(stderr, "Error getting file entry %u: %u\n", i, err);
					return 4;
				}
				printf("%u: %s\n", i, subfile);
			}

			if (file.file_size > 0) {
				printf("Choose entry ([0-%" PRIu64 "], anything else quits): ", file.file_size - 1);
				unsigned long response = 0;
				if (scanf("%lu", &response) != 1) {
					return 5;
				}
				if (response >= file.file_size) {
					return 5;
				}

				err = fat_list(&file, response, subfile, sizeof(subfile));
				if (err != FAT_OK) {
					fprintf(stderr, "Error getting file entry %lu: %u\n", response, err);
					return 4;
				}
				strncat(filename, "/", sizeof(filename) - strlen(filename));
				strncat(filename, subfile, sizeof(filename) - strlen(filename));
			} else {
				running = 0;
			}
		} else {
			printf("Found file \"%s\" at cluster %u, size %lu bytes\n", filename, file.start_cluster, file.file_size);

			EVP_MD_CTX *ctx = EVP_MD_CTX_new();
			EVP_DigestInit_ex(ctx, EVP_sha256(), NULL);

			for (uint64_t offset = 0; offset < file.file_size;) {
				uint8_t data[4096];
				uint16_t length;
				if (file.file_size - offset > sizeof(data)) {
					length = sizeof(data);
				} else {
					length = file.file_size - offset;
				}
				err = fat_read(&file, offset, length, &data);
				if (err != FAT_OK) {
					fprintf(stderr, "Error reading %u bytes from offset %lu: %u\n", length, offset, err);
					return 3;
				}
				EVP_DigestUpdate(ctx, data, length);
				offset += length;
			}

			unsigned char digest[EVP_MAX_MD_SIZE];
			unsigned int digestlen = 0;
			EVP_DigestFinal_ex(ctx, digest, &digestlen);
			for (size_t i = 0; i < digestlen; i++) {
				printf("%02x", digest[i]);
			}
			printf("  %s\n", filename);
			EVP_MD_CTX_free(ctx);

			running = 0;
		}
	}

	fclose(fp);
	return 0;
}
