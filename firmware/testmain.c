/*
 *  Copyright © 2023 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include "uart.h"
#include "spi.h"
#include "gpio.h"
#include "ymf.h"
#include "hsc.h"
#include "clock.h"

static void test() {
	// test - play a simple sound
	ymf_begin();
	// enable output channel A + B on instrument 0, additive 2-op mode
	//ymf_write_reg(0, 0xc0, 0x30);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(0), FM_BIT_CHA | FM_BIT_CHB | FM_BIT_CNT);
	// set frequency multiplier on intrument 0 operator 1 to 1, key scale off, tremolo off, vibrato off, sustain off
	//ymf_write_reg(0, 0x20, 0x01);
	ymf_write_reg_com(FM_REG_AM_VIB_EGT_KSR_MULT_1(0), FM_MAKE_MULT(1));
	// set volume (attenuation?) on instrument 0, operator 1 to 16*-0.75dB=-12dB
	//ymf_write_reg(0, 0x40, 0x10);
	ymf_write_reg_com(FM_REG_KSL_TL_1(0), FM_MAKE_TL(16));
	// set attack rate of instrument 0, operator 1 to max, decay rate to min, sustain level to half (7*-3dB) and release rate to half
	//ymf_write_reg(0, 0x60, 0xf0);
	//ymf_write_reg(0, 0x80, 0x77);
	ymf_write_reg_com(FM_REG_AR_DR_1(0), FM_MAKE_AR(15));
	ymf_write_reg_com(FM_REG_SL_RR_1(0), FM_MAKE_SL(7) | FM_MAKE_RR(7));
	// frequency (max = 0x3ff), 0x241 = 440Hz -> close enough, should be 0x244
	uint16_t freq = 0x241;
	// set low 8 frequency bits of instrument 0
	//ymf_write_reg(0, 0xa0, 0x98);
	ymf_write_reg_com(FM_F_NUMBERL_1(0), FM_MAKE_FNUML(freq));
	// set frequency multiplier on intrument 0 operator 4 to 1, key scale off, tremolo off, vibrato off, sustain off
	//ymf_write_reg(0, 0x23, 0x01);
	ymf_write_reg_com(FM_REG_AM_VIB_EGT_KSR_MULT_1(3), FM_MAKE_MULT(1));
	// set volume on instrument 0, operator 4 to 63=-47dB
	//ymf_write_reg(0, 0x43, 0x00);
	ymf_write_reg_com(FM_REG_KSL_TL_1(3), FM_MAKE_TL(63));
	// set attack rate of instrument 0, operator 4 to max, decay rate to min, sustain level to half and release rate to half
	//ymf_write_reg(0, 0x63, 0xf0);
	//ymf_write_reg(0, 0x83, 0x77);
	ymf_write_reg_com(FM_REG_AR_DR_1(3), FM_MAKE_AR(15));
	ymf_write_reg_com(FM_REG_SL_RR_1(3), FM_MAKE_SL(7) | FM_MAKE_RR(7));
	// set block (octave) 4 and top 2 bits of frequency on instrument 0, key on
	//ymf_write_reg(0, 0xb0, 0x31);
	ymf_write_reg_com(FM_REG_KON_BLOCK_FNUMH_1(0), FM_BIT_KON | FM_MAKE_BLOCK(4) | FM_MAKE_FNUMH(freq));
	ymf_end();
}

static void test2() {
	// Test code from https://www.vogons.org/viewtopic.php?t=55181
	// Produces a stereo sine wave at 440Hz with a fade-in on a real OPL.
	// The fade-in is not very smooth, steps can be heard clearly.
	// Some clones exhibit other behavior.

	// printf("Reset chip\n");
	for (uint16_t i = 0x20; i < 0xff; i++) {
		if ((i & 0xe0) == 0x80) {
			ymf_write_reg_com(i, 0x0f);
		} else {
			ymf_write_reg_com(i, 0x00);
		}
	}
	_delay_ms(40);

	// printf("Set patch\n");
	ymf_write_reg_com(0x20, 0x03);
	ymf_write_reg_com(0x23, 0x01);
	ymf_write_reg_com(0x40, 0x2f);
	ymf_write_reg_com(0x43, 0x00);
	ymf_write_reg_com(0x61, 0x10);
	ymf_write_reg_com(0x63, 0x10);
	ymf_write_reg_com(0x80, 0x00);
	ymf_write_reg_com(0x83, 0x00);
	ymf_write_reg_com(0xa0, 0x44);
	ymf_write_reg_com(0xb0, 0x12);
	ymf_write_reg_com(0xc0, 0xfe);
	// printf("Start voice\n");
	ymf_write_reg_com(0xb0, 0x32);
	_delay_ms(1);
	ymf_write_reg_com(0x60, 0xf0);

	// getch()
	_delay_ms(5000);

	// printf("Reset chip\n");
	for (uint16_t i = 0x20; i < 0xff; i++) {
		if ((i & 0xe0) == 0x80) {
			ymf_write_reg_com(i, 0x0f);
		} else {
			ymf_write_reg_com(i, 0x00);
		}
	}
}

static void init() {
	uart_init();
	gpio_init();
	clock_init();

	// some startup delay
	_delay_ms(200);

	ymf_begin();
	uint8_t opl = ymf_detect();
	switch (opl) {
		case YMF_DETECT_NONE:
			puts_P(PSTR("No OPL detected!"));
			break;
		case YMF_DETECT_OPL2:
			puts_P(PSTR("OPL2 detected!"));
			break;
		case YMF_DETECT_OPL3:
			puts_P(PSTR("OPL3 detected (maybe)!"));
			break;
		default:
			puts_P(PSTR("Invalid ymf_detect return code"));
			break;
	}
	// enable OPL3 mode
	/*ymf_write_reg_com(FM_REG_NEW, FM_BIT_NEW);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(0), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(1), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(2), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(3), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(4), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(5), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(6), FM_BIT_CHA | FM_BIT_CHB);
	ymf_write_reg_com(FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(7), FM_BIT_CHA | FM_BIT_CHB);*/
	ymf_end();
}

static void loop() {
	ymf_begin();
	song_init();

	puts_P(PSTR("Playing song..."));
	bool running = true;
	while (running) {
		//ymf_begin();
		running = song_update();
		//ymf_end();
		clock_wait(CLK_STEPS_US(DT_NOTE_US));
	}
	puts_P(PSTR("done."));

	ymf_end();
}

int main() {
	init();
	while (1) {
		loop();
		//test();
		//test2();
		_delay_ms(1000);
	}
}
