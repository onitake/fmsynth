/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "fat.h"
#include "fat_struct.h"
#include "fat_private.h"
#ifdef FAT_PATENTED
#include "fat_patented.h"
#endif
#ifdef FAT_DEBUG
#include "fat_debug.h"
#endif

fat_read_block_t *fat_read_func;
uint8_t *fat_cached_block;
uint32_t fat_cached_blockid;
uint32_t fat_boot_blockid;
uint32_t fat_partition_size;
uint32_t fat_offset;
uint32_t fat_length;
uint32_t fat_cluster_offset;
uint32_t fat_cluster_count;
uint32_t fat_cluster_shift;
uint32_t fat_cluster_mask;
uint32_t fat_root_cluster;
uint32_t fat_selected_entries;
uint32_t fat_selected_cluster;
uint32_t fat_selected_cluster_block;

void fat_read_helper(uint32_t blockid) {
	if (fat_cached_block == NULL || blockid != fat_cached_blockid) {
		fat_cached_block = fat_read_func(blockid);
		fat_cached_blockid = blockid;
	}
}

fat_error_t fat_parse_mbr() {
	// read MBR boot block
	fat_read_helper(FAT_MBR_BLOCKID);
	if (!fat_cached_block) return FAT_ERROR_READ;
	fat_mbr_t *fat_mbr = (fat_mbr_t *) fat_cached_block;
#ifdef FAT_DEBUG
	fat_debug_mbr(fat_mbr);
#endif
	// some minor sanity checks
	if (fat_mbr->bootsignature[0] != FAT_MBR_SIG0 || fat_mbr->bootsignature[1] != FAT_MBR_SIG1) return FAT_ERROR_MBR;
	if (fat_mbr->partition[0].type != FAT_MBR_TYPE_EXFAT) return FAT_ERROR_NOEXFAT;
	// load LBA of first partition
	fat_boot_blockid = fat_mbr->partition[0].lbafirst;
	fat_partition_size = fat_mbr->partition[0].sectors;
	return FAT_OK;
}

fat_error_t fat_parse_boot() {
	// read MBR boot block
	fat_read_helper(fat_boot_blockid + FAT_BOOT_OFFSET);
	if (!fat_cached_block) return FAT_ERROR_READ;
	fat_boot_sector_t *fat_main_boot = (fat_boot_sector_t *) fat_cached_block;
#ifdef FAT_DEBUG
	fat_debug_boot_sector(fat_main_boot);
#endif
	// FIXME we only support 512-byte blocks
	if (fat_main_boot->BytesPerSectorShift != 9) return FAT_ERROR_SECTORSIZE;
	// FS revision check
	if (fat_main_boot->FileSystemRevision.High != FAT_SUPPORTED_MAJOR_VERSION) return FAT_ERROR_VERSION;
	// TODO we do not support multiple FATs for now
	if (fat_main_boot->NumberOfFats != 1) return FAT_ERROR_TWOFAT;
	// ensure the primary FAT is active
	if (fat_main_boot->VolumeFlags.ActiveFat != 0) return FAT_ERROR_TWOFAT;
	// TODO test cluster_chain[0] == FAT_ENTRY_MEDIA_TYPE_EXFAT
	// store FS information from boot sector
	fat_offset = fat_main_boot->FatOffset;
	fat_length = fat_main_boot->FatLength;
	fat_cluster_offset = fat_main_boot->ClusterHeapOffset;
	fat_cluster_count = fat_main_boot->ClusterCount;
	fat_cluster_shift = fat_main_boot->SectorsPerClusterShift;
	fat_cluster_mask = (1 << fat_main_boot->SectorsPerClusterShift) - 1;
	fat_root_cluster = fat_main_boot->FirstClusterOfRootDirectory;
	return FAT_OK;
}

fat_error_t fat_find_next_cluster(uint32_t number, uint32_t *clusterid) {
	// calculate local offsets
	uint32_t block = number / FAT_ENTRIES_PER_BLOCK;
	uint32_t index = number % FAT_ENTRIES_PER_BLOCK;
	// bounds checking, the size of the FAT is limited
	if (block >= fat_length) return FAT_ERROR_OUTOFBOUNDS;
	// read related MBR boot block
	fat_read_helper(fat_boot_blockid + fat_offset + block);
	if (!fat_cached_block) return FAT_ERROR_READ;
	fat_entries_t *fat_entries = (fat_entries_t *) fat_cached_block;
#ifdef FAT_DEBUG
	fat_debug_chain(fat_entries);
#endif
	uint32_t id = fat_entries->FatEntry[index];
	if (id == FAT_ENTRY_BAD) {
		return FAT_ERROR_BADCLUSTER;
	}
	// also covers FAT_ENTRY_LAST
	if (id < 2 || id > fat_cluster_count + 1) {
		return FAT_ERROR_OUTOFBOUNDS;
	}
	*clusterid = id;
	return FAT_OK;
}

fat_error_t fat_load_cluster_block(uint32_t startcluster, uint32_t blocknumber, bool nofatchain) {
	fat_error_t err = FAT_OK;
	uint32_t clusteridx = blocknumber >> fat_cluster_shift;
	uint32_t localblock = blocknumber & fat_cluster_mask;
	uint32_t cluster = startcluster;
	if (nofatchain) {
		// linear allocation, just add the index
		cluster += clusteridx;
	} else {
		// chained allocation, jump through the linked list
		for (uint32_t i = 0; i < clusteridx; i++) {
			err = fat_find_next_cluster(cluster, &cluster);
			if (err != FAT_OK) return err;
		}
	}
	// bounds checking
	if (cluster >= fat_cluster_count) return FAT_ERROR_OUTOFBOUNDS;
	// calculate cluster offset
	// need to subtract 2 because the first two clusters are used as flags in the chain table and don't exist on disk
	uint32_t globalblock = ((cluster - 2) << fat_cluster_shift);
	fat_read_helper(fat_boot_blockid + fat_cluster_offset + globalblock + localblock);
	if (!fat_cached_block) return FAT_ERROR_READ;
	return err;
}

fat_error_t fat_load_directory_entry(uint32_t startcluster, uint32_t number, fat_dir_entry_t **entry, bool nofatchain) {
	fat_error_t err = FAT_OK;
	// calculate local offsets
	uint32_t block = number / FAT_DIR_ENTRIES_PER_BLOCK;
	uint32_t index = number % FAT_DIR_ENTRIES_PER_BLOCK;
	err = fat_load_cluster_block(startcluster, block, nofatchain);
	if (err != FAT_OK) return err;
	fat_dir_entries_t *fat_cluster = (fat_dir_entries_t *) fat_cached_block;
	*entry = &fat_cluster->DirectoryEntry[index];
	return err;
}

fat_error_t fat_init(fat_read_block_t *read) {
	fat_error_t err = FAT_OK;
	fat_read_func = read;
	fat_cached_block = NULL;
	fat_boot_blockid = 0;

	err = fat_parse_mbr();
	if (err != FAT_OK) return err;
	err = fat_parse_boot();
	if (err != FAT_OK) return err;
#ifdef FAT_PATENTED
	err = fat_verify_boot_checksum();
	if (err != FAT_OK) return err;
#endif
#ifdef FAT_DEBUG
	fat_debug_root_directory();
#endif
	return err;
}

// `data` will be used both as input and as output
fat_error_t fat_find_file(const char *name, uint8_t namelen, fat_file_t *data) {
	// copy the inputs to avoid overwriting them
	uint32_t startcluster = data->start_cluster;
	// is the data for this entry linearly aligned or a series of chained clusters?
	bool nofatchain = data->no_fat_chain;
	// we count by number of direntries, not bytes
	// and we always use the maximum number entries, then search for the end marker
	uint32_t direntries = FAT_MAX_DIRECTORY_SIZE / sizeof(fat_dir_entry_t);
	// if we were called with an empty name:
	// this is a directory open request and we've reached the leaf - scan the directory contents
	// instead of searching for a file, but count the number of file entries
	data->file_size = 0;
	// FIXME use case insensitive matching via name hash and upcase table (or hard-coded conversion)
	// temp store entry parameters
	uint8_t nlen = 0;
	uint8_t nidx = 0;
	uint8_t nsec = 0;
	// loop over all directory entries in this cluster chain
	for (uint32_t i = 0; i < direntries; i++) {
		fat_dir_entry_t *entry = NULL;
		fat_load_directory_entry(startcluster, i, &entry, nofatchain);
		// skip entries that aren't in use
		if (entry->EntryType.InUse == 1) {
			// primary or secondary?
			if (entry->EntryType.TypeCategory == 0) {
				// do we have a file entry?
				if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_FILE && entry->EntryType.TypeImportance == 0) {
					// is this a file or directory scan?
					if (*name) {
						// store number of secondary entries and some other data, in case that this is a match
						fat_file_dir_entry_t *fentry = (fat_file_dir_entry_t *) entry;
						nsec = fentry->SecondaryCount;
						data->flags.ro = fentry->FileAttributes.ReadOnly;
						data->flags.hidden = fentry->FileAttributes.Hidden;
						data->flags.system = fentry->FileAttributes.System;
						data->flags.directory = fentry->FileAttributes.Directory;
						data->flags.archive = fentry->FileAttributes.Archive;
					} else {
						// directory scan, just increment the entry counter
						data->file_size++;
					}
				}
			} else {
				// secondary entry, verify that we're expecting it (part of a file entry)
				// but only if it's a file scan
				if (*name && nsec > 0) {
					// supported secondary entry types
					if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_STREAM_EXTENSION && entry->EntryType.TypeImportance == 0) {
						// StreamExtension (actual data belonging to this file)
						fat_stream_extension_dir_entry_t *fentry = (fat_stream_extension_dir_entry_t *) entry;
						// check if the file name length matches, skip otherwise
						if (fentry->NameLength == namelen) {
							// copy values from stream extension, in case this is really a match
							data->file_size = fentry->ValidDataLength;
							data->start_cluster = fentry->FirstCluster;
							data->no_fat_chain = fentry->GeneralSecondaryFlags.NoFatChain != 0;
							nlen = fentry->NameLength;
							nidx = 0;
						}
					} else if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_FILE_NAME && entry->EntryType.TypeImportance == 0) {
						// FileName (multiple entries possible, concatenate to form file name)
						fat_file_name_dir_entry_t *fentry = (fat_file_name_dir_entry_t *) entry;
						// compare next file name segment
						for (uint8_t i = 0; i < sizeof(fentry->FileName)/sizeof(fentry->FileName[0]) && nidx < nlen; i++, nidx++) {
							// TODO this is a trivial case-sensitive Latin-1 lookup.
							// According to the specification, this should be a case insensitive Unicode compare,
							// using the upcase table to convert file names to upper case.
							// Microsoft does not mention it explicitly in the exFAT specification, but "Unicode"
							// should mean "UTF-16" in this context. This is not of much relevance for Latin-1 file names,
							// but things will get more complicated if POSIX wchar_t is supported - which is UTF-32...
							// Another possibility would be to limit the character set to UCS-2 and simply forget about
							// emoji and other higher codepoints than 0xffff.
							if (fentry->FileName[i] != name[nidx]) {
								// mismatch, stop here and move to the next File entry (1, because we're decrementing nsec below)
								nsec = 1;
								break;
							}
							// if the comparison is complete now, we have a match
							if (nidx == nlen - 1) {
								return FAT_OK;
							}
						}
					}
					// one secondary entry used up
					nsec--;
				}
				// other secondary entries are skipped
			}
		} else {
			// honor the end of directory marker
			// this is particularly important for the root directory (no size...)
			if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_END && entry->EntryType.TypeImportance == 0 && entry->EntryType.TypeCategory == 0) break;
		}
	}
	if (*name) {
		// if this was a file search, we've reached the end without a match
		return FAT_ERROR_NOTFOUND;
	} else {
		// if it was a directory scan, we're done
		return FAT_OK;
	}
}

fat_error_t fat_open(const char *path, fat_file_t *file) {
	fat_error_t err = FAT_OK;

	// sanity check
	if (!file) return FAT_ERROR_INVALIDARG;

	// work down the directory hierarchy, starting at the root entry
	// set defaults for the root directory
	file->start_cluster = fat_root_cluster;
	// the root directory size isn't specified, so we use the maximum and just bail out
	// when reaching the end marker
	file->file_size = FAT_MAX_DIRECTORY_SIZE;
	file->no_fat_chain = false;
	file->flags.ro = 0;
	file->flags.hidden = 0;
	file->flags.system = 0;
	file->flags.directory = 1;
	file->flags.archive = 0;

	// skip leading slashes
	while (*path == '/' || *path == '\\') {
		path++;
	}

	// we do not impose an upper limit to the total path length - lookup is done linearly
	while (*path) {
		// find end of path marker
		// we do limit the length of one path component to 255 here
		// TODO maybe we could do this in fat_find_file on the fly to save some cycles?
		uint8_t namelen = 0;
		while (namelen < 255 && path[namelen] && path[namelen] != '/' && path[namelen] != '\\') {
			namelen++;
		}
		// test if the path component limit was exceeded
		if (namelen == 255 && *path && *(path + namelen + 1)) return FAT_ERROR_PATHLEN;

		// search for a matching entry
		err = fat_find_file(path, namelen, file);
		if (err != FAT_OK) return err;

		// skip forward to the next entry
		path += namelen;

		// skip trailing slashes
		while (*path == '/' || *path == '\\') {
			path++;
		}
	}

	// if we found a directory as the leaf, gather the number of files in it
	if (file->flags.directory) {
		err = fat_find_file(path, 0, file);
	}

	return err;
}

fat_error_t fat_read(const fat_file_t *file, uint64_t offset, uint16_t length, void *data) {
	fat_error_t err = FAT_OK;

	// sanity checks
	if (!file) return FAT_ERROR_INVALIDARG;
	if (file->flags.directory) return FAT_ERROR_INAPPROPRIATE;
	// this is to ensure we don't exceed the block number
	if (offset > FAT_MAX_FILE_SIZE) return FAT_ERROR_OUTOFBOUNDS;
	// adding these is actually safe, because FAT_MAX_FILE_SIZE will never exceed UINT64_MAX-UINT16_MAX
	if (offset + length > file->file_size) return FAT_ERROR_OUTOFBOUNDS;

	// TODO implement this as a zero-copy function instead, returning a pointer to the data buffer and
	// returning the number of bytes available in *length.

	while (length > 0) {
		uint32_t block = (uint32_t) (offset / FAT_BLOCK_SIZE);
		uint32_t index = (uint32_t) (offset % FAT_BLOCK_SIZE);

		err = fat_load_cluster_block(file->start_cluster, block, file->no_fat_chain);
		if (err != FAT_OK) return err;

		// limit to the block boundary
		uint16_t toread;
		if (length > FAT_BLOCK_SIZE - index) {
			toread = FAT_BLOCK_SIZE - index;
		} else {
			toread = length;
		}

#ifdef FAT_NO_MEMCPY
		for (uint16_t i = 0; i < toread; i++) {
			((uint8_t *) data)[i] = fat_cached_block[index + i];
		}
#else
		memcpy(data, &fat_cached_block[index], toread);
#endif

		length -= toread;
		data += toread;
		offset += toread;
	}

	return err;
}

fat_error_t fat_list(const fat_file_t *data, uint32_t index, char *filename, uint8_t namelen) {
	if (!data || !filename || !namelen || !data->flags.directory) {
		return FAT_ERROR_INVALIDARG;
	}
	// TODO

	// copy the inputs to avoid overwriting them
	uint32_t startcluster = data->start_cluster;
	// is the data for this entry linearly aligned or a series of chained clusters?
	bool nofatchain = data->no_fat_chain;
	// we count by number of direntries, not bytes
	// and we always use the maximum number entries, then search for the end marker
	uint32_t direntries = FAT_MAX_DIRECTORY_SIZE / sizeof(fat_dir_entry_t);
	// counter for primary file entries
	uint32_t nentry = 0;
	// number of secondary entries connected to this entry
	uint8_t nsec = 0;
	// keep the primary entry index
	uint32_t pidx;
	// loop over all directory entries in this cluster chain
	for (pidx = 0; pidx < direntries; pidx++) {
		fat_dir_entry_t *entry = NULL;
		fat_load_directory_entry(startcluster, pidx, &entry, nofatchain);
		// skip entries that aren't in use
		if (entry->EntryType.InUse == 1) {
			// primary or secondary?
			if (entry->EntryType.TypeCategory == 0) {
				// do we have a file entry?
				if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_FILE && entry->EntryType.TypeImportance == 0) {
					// store number of secondary entries
					fat_file_dir_entry_t *fentry = (fat_file_dir_entry_t *) entry;
					nsec = fentry->SecondaryCount;
					// is this the one we're looking for? -> abort
					if (nentry == index) break;
					// nope, skip to the file next entry
					pidx += nsec;
					nentry++;
				}
			}
		} else {
			// honor the end of directory marker
			// this is particularly important for the root directory (no size...)
			if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_END && entry->EntryType.TypeImportance == 0 && entry->EntryType.TypeCategory == 0) return FAT_ERROR_OUTOFBOUNDS;
		}
	}

	// some helpers for data spread over multiple entries
	uint8_t nidx = 0;
	uint8_t nlen = 0;

	for (uint8_t sidx = 1; sidx <= nsec; sidx++) {
		fat_dir_entry_t *entry = NULL;
		fat_load_directory_entry(startcluster, pidx + sidx, &entry, nofatchain);
		// supported secondary entry types
		if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_STREAM_EXTENSION && entry->EntryType.TypeImportance == 0) {
			// StreamExtension (actual data belonging to this file)
			fat_stream_extension_dir_entry_t *fentry = (fat_stream_extension_dir_entry_t *) entry;
			// save the filename length
			nlen = fentry->NameLength;
			nidx = 0;
		} else if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_FILE_NAME && entry->EntryType.TypeImportance == 0) {
			// FileName (multiple entries possible, concatenate to form file name)
			fat_file_name_dir_entry_t *fentry = (fat_file_name_dir_entry_t *) entry;
			// copy the next name fragment
			// leave space for the null terminator
			for (uint8_t i = 0; i < sizeof(fentry->FileName)/sizeof(fentry->FileName[0]) && nidx < nlen && nidx < namelen - 1; i++, nidx++) {
				// This is a trivial UCS-2-to-Latin-1 conversion, with the top 8 bits truncated.
				// FIXME We should do better, if possible.
				filename[nidx] = (uint8_t) fentry->FileName[i];
			}
			// if we've reached the end now, we're done
			if (nidx == nlen || nidx == namelen - 1) {
				// add the null terminator
				filename[nidx] = 0;
				return FAT_OK;
			}
		}
		// other secondary entries are skipped
	}

	// we didn't get a filename - index was out of bounds or some other problem occurred
	return FAT_ERROR_NOTFOUND;
}