/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FAT_DEBUG_H
#define _FAT_DEBUG_H

#include <stdint.h>
#include "fat_struct.h"

#ifdef __cplusplus
extern "C" {
#endif

void fat_debug_mbr(fat_mbr_t *fat_mbr);
void fat_debug_boot_sector(fat_boot_sector_t *fat_main_boot);
void fat_debug_chain(fat_entries_t *fat_entries);
void fat_debug_dir_entry(fat_dir_entry_t *entry);
void fat_debug_volume_label(fat_volume_label_dir_entry_t *entry);
void fat_debug_file_entry(fat_file_dir_entry_t *entry);
void fat_debug_allocation_bitmap(fat_allocation_bitmap_dir_entry_t *entry);
void fat_debug_upcase_table(fat_upcase_table_dir_entry_t *entry);
void fat_debug_stream_extension(fat_stream_extension_dir_entry_t *entry);
void fat_debug_file_name(fat_file_name_dir_entry_t *entry);
void fat_debug_volume_guid(fat_volume_guid_dir_entry_t *entry);
void fat_debug_vendor_extension(fat_vendor_extension_dir_entry_t *entry);
void fat_debug_vendor_allocation(fat_vendor_allocation_dir_entry_t *entry);
void fat_debug_any_entry(fat_dir_entry_t *entry);
fat_error_t fat_debug_root_directory();

#ifdef __cplusplus
}
#endif

#endif /*_FAT_DEBUG_H*/
