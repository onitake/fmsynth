/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "sd.h"
#include "spi.h"
#include "crc.h"
#include "gpio.h"

#ifdef SD_DEBUG
#include <stdio.h>
#include <avr/pgmspace.h>
#define sd_debug(msg, ...) printf_P(PSTR(msg),##__VA_ARGS__)
#else
#define sd_debug(msg, ...)
#endif


// SPI clock divider used during the setup phase
#define SD_SETUP_CLOCK_DIV SPI_CLK_DIV_64
// SPI clock divider during data transfer
#define SD_DATA_CLOCK_DIV SPI_CLK_DIV_2

#if SPI_CLOCK(SD_SETUP_CLOCK_DIV) < 100000 || SPI_CLOCK(SD_SETUP_CLOCK_DIV) > 400000
#warning SPI setup clock divider is out of range (must be within F_CPU/100kHz..F_CPU/400kHz)
#endif

// TXB0104 needs 1µs to enable all drivers
#define SD_TCB0104_ENABLE_DELAY_US 1.0
// TXB0104 @VCCA=3.3V,VCCB=5V: max. disable time = 13.2ns
#define SD_TCB0104_DISABLE_DELAY_US 0.0132

// Timings in number of SPI transfers
// Max. time to wait before R1 after command
#define SD_TIMEOUT_RESPONSE_R1 8
// Max. time to wait for card init
// TODO Experimentally determined, cross-check with spec
#define SD_TIMEOUT_CARD_INIT 128
// Max. time to wait for a read block response
// The card should specify this in its CSD register, this is the default if not available
#define SD_TIMEOUT_BLOCK_READ_DEFAULT 2048
// Number of dummy transfers for card init (with SDCS deasserted)
// Actual minimum is 74 SPI clocks, but it's easier to just to 80 (10*8)
#define SD_INIT_DUMMY_TRANSFERS 10

// Maximum block address for standard capacity cards
#define SD_MAX_BLOCK_ADDRESS_SC ((uint32_t) (0x100000000UL / SD_BLOCK_SIZE))

// DIN/DOUT=high dummy transfer or response, also the dummy CRC
#define SD_DUMMY_HIGH 0xff

// start token for single/multi read or single write
#define SD_SINGLE_START_TOKEN 0xfe

// SD card SPI protocol error bits (R1 response)
#define SD_R1_MASK 0x7f
#define SD_R1_OK_MASK 0x7e
#define SD_R1_IDLE (1 << 0)
#define SD_R1_ERASE_RESET (1 << 1)
#define SD_R1_ILLEGAL_CMD (1 << 2)
#define SD_R1_CRC_ERR (1 << 3)
#define SD_R1_ERASE_SEQ_ERR (1 << 4)
#define SD_R1_ADDRESS_ERR (1 << 5)
#define SD_R1_PARM_ERR (1 << 6)
// Additional bits for R1b
#define SD_R1B_OK_MASK 0xfe
#define SD_R1B_BUSY (1 << 7)

// parameters for CMD8 (SEND_IF_COND)
#define SD_CMD8_VOLTAGE_3V 0x01
#define SD_CMD8_VOLTAGE_LOW 0x02
#define SD_CMD8_PCIE_1V2 0x20
#define SD_CMD8_PCIE 0x10
#define SD_CMD8_PATTERN 0x55

// parameters for CMD59 (CRC_ON_OFF)
#define SD_CMD59_CRC_ON 0x01

// parameters for ACMD41 (SD_SEND_OP_COND)
#define SD_ACMD41_HCS (1 << 6)

// responses for CMD58 (READ_OCR)
#define SD_CMD58_BUSY (1UL << 31)
#define SD_CMD58_CCS (1UL << 30)
#define SD_CMD58_UHS2 (1UL << 29)
#define SD_CMD58_CO2T (1UL << 27)
#define SD_CMD58_S18R (1UL << 24)
#define SD_CMD58_VDD_27_28 (1UL << 15)
#define SD_CMD58_VDD_28_29 (1UL << 16)
#define SD_CMD58_VDD_29_30 (1UL << 17)
#define SD_CMD58_VDD_30_31 (1UL << 18)
#define SD_CMD58_VDD_31_32 (1UL << 19)
#define SD_CMD58_VDD_32_33 (1UL << 20)
#define SD_CMD58_VDD_33_34 (1UL << 21)
#define SD_CMD58_VDD_34_35 (1UL << 22)
#define SD_CMD58_VDD_35_36 (1UL << 23)
#define SD_CMD58_DUAL_VOLT (1UL << 7)


static struct {
	bool initialized;
	bool asserted;
	uint8_t card_type;
	uint16_t read_timeout;
} sd_state = {
	.initialized = false,
	.asserted = false,
};


// read an R1 or R1b response (1 byte)
uint8_t sd_receive_r1_r1b() {
	// wait until busy flag clear or timeout (more than 8 dummy bytes)
	uint8_t cr;
	uint8_t response = SD_GENERAL_ERROR;
	// +1, because this includes the response byte
	for (cr = 0; response == SD_DUMMY_HIGH && cr < (SD_TIMEOUT_RESPONSE_R1 + 1); cr++) {
		response = spi_transfer(SD_DUMMY_HIGH);
	}
	// more than than the expected dummy bytes indicates timeout
	if (cr >= (SD_TIMEOUT_RESPONSE_R1 + 1)) {
		return SD_GENERAL_ERROR;
	}
	// guard transfer before next command
	spi_transfer(SD_DUMMY_HIGH);
	return response;
}

// read an R7 or R3 response (status byte + 4 data bytes)
// the status byte is returned directly, the data bytes will be stored in the argument
uint8_t sd_receive_r3_r7(uint8_t response[4]) {
	// read the dummy bytes and status byte first
	uint8_t cr;
	uint8_t err = SD_GENERAL_ERROR;
	// 9, because the maximum is 8 busy bytes + 1 response byte
	for (cr = 0; err == SD_DUMMY_HIGH && cr < (SD_TIMEOUT_RESPONSE_R1 + 1); cr++) {
		err = spi_transfer(SD_DUMMY_HIGH);
	}
	// more than 8 (7+1) busy bytes indicates timeout
	if (cr >= (SD_TIMEOUT_RESPONSE_R1 + 1)) {
		return SD_GENERAL_ERROR;
	}
	// we ignore the R1 code here and always read the whole response
	// it needs to be evaluated by the caller
	// read the rest of the response
	for (uint8_t i = 0; i < 4; i++) {
		response[i] = spi_transfer(0xff);
	}
	// guard transfer before next command
	spi_transfer(SD_DUMMY_HIGH);
	return err;
}

// Data is transferred most significant bit first, but most significant byte last.
// (Physical Layer Simplified Specification Version 8.00, chapter 3.6.1, figure 3-9)
// It defines the bit sequence on wire as follows (s=start, d=dir, c=cmd, a=arg, r=crc, t=stop):
// sdcccccc aaaaaaaa aaaaaaaa aaaaaaaa aaaaaaaa rrrrrrrt
typedef struct {
	uint8_t data[6];
} __attribute__((__packed__)) sd_cmd_t;

// send a normal command
// if usecrc is true, calculates and sends the correct CRC with the command,
// replaces it with the dummy value 0xff otherwise
// important: this does not read the response, call the sd_receive_* function
// appropriate for the command next
void sd_send_cmd(sd_cmd_t *command, bool usecrc) {
	sd_debug("send command %u\n", command->data[0] & ~0xc0);
	// set start (0) and direction bits (1)
	command->data[0] = (command->data[0] & ~0xc0) | 0x40;
	if (usecrc) {
		// calculate crc and append stop bit
		command->data[5] = (sd_crc7(command->data, 5) << 1) | 0x01;
	} else {
		command->data[5] = SD_DUMMY_HIGH;
	}
	for (uint8_t i = 0; i < sizeof(command->data); i++) {
		spi_transfer(command->data[i]);
	}
	// no guard transfer here, this needs to be done in sd_receive_* or
	// before data transmission
}

// send a application-specific command
// this will send the CMD55 command, read the response, then send the ACMD
// from the argument.
// the response needs to be read separately, like for sd_send_cmd.
// returns the R1 response from CMD55, or SD_R1_IDLE if successful
uint8_t sd_send_acmd(sd_cmd_t *command, bool usecrc) {
	// send APP_CMD (CMD55) first
	sd_cmd_t cmd55 = { { 55, 0x00, 0x00, 0x00, 0x00, 0x00 } };
	sd_send_cmd(&cmd55, usecrc);
	uint8_t err = sd_receive_r1_r1b();
	sd_debug("response %02x\n", err);
	if ((err & SD_R1_OK_MASK) != 0) {
		return err;
	}
	// send command
	sd_send_cmd(command, usecrc);
	return err;
}

// CMD0: card reset
// returns an R1 response, expected answer is SD_R1_IDLE
uint8_t sd_send_cmd0() {
	sd_cmd_t cmd0 = { { 0, 0x00, 0x00, 0x00, 0x00, 0x00 } };
	sd_send_cmd(&cmd0, true);
	uint8_t response = sd_receive_r1_r1b();
 	sd_debug("response %02x\n", response);
	return response;
}

// CMD8: set voltage and initialize card
uint8_t sd_send_cmd8() {
	sd_cmd_t cmd8 = { { 8, 0x00, 0x00, SD_CMD8_VOLTAGE_3V, SD_CMD8_PATTERN, 0x00 } };
	sd_send_cmd(&cmd8, true);
	uint8_t response[4] = { 0, 0, 0, 0 };
	uint8_t err = sd_receive_r3_r7(response);
	sd_debug("response %02x\n", err);
	if ((err & SD_R1_OK_MASK) != 0) {
		return err;
	}
	sd_debug("if %02x %02x %02x %02x\n", response[0], response[1], response[2], response[3]);
	// check pattern match?
	if (response[3] != SD_CMD8_PATTERN) {
		sd_debug("error pattern %02x\n", response[3]);
		return SD_GENERAL_ERROR;
	}
	// voltage match?
	if ((response[2] & SD_CMD8_VOLTAGE_3V) == 0) {
		sd_debug("error voltage 3v not set %02x\n", response[2]);
		return SD_GENERAL_ERROR;
	}
	return err;
}

// CMD16: set block size in SDCS mode
uint8_t sd_send_cmd16(uint32_t size) {
	sd_cmd_t cmd16 = { { 16, (size >> 24) & 0xff, (size >> 16) & 0xff, (size >> 8) & 0xff, size & 0xff, 0x00 } };
	sd_send_cmd(&cmd16, false);
	uint8_t err = sd_receive_r1_r1b();
	sd_debug("response %02x\n", err);
	return err;
}

// CMD17: read one block of data
// note: in high capacity (HCS) cards, this is the block address, while in
// standard capacity cards, it's a byte address
uint8_t sd_send_cmd17(uint32_t address) {
	sd_cmd_t cmd17 = { { 17, (address >> 24) & 0xff, (address >> 16) & 0xff, (address >> 8) & 0xff, address & 0xff, 0x00 } };
	sd_send_cmd(&cmd17, false);
	uint8_t err = sd_receive_r1_r1b();
	sd_debug("response %02x\n", err);
	return err;
}

// CMD58: read OCR register
uint8_t sd_send_cmd58(uint32_t *ocr) {
	if (!ocr) {
		return SD_GENERAL_ERROR;
	}
	sd_cmd_t cmd58 = { { 58, 0x00, 0x00, 0x00, 0x00, 0x00 } };
	sd_send_cmd(&cmd58, false);
	uint8_t response[4] = { 0, 0, 0, 0 };
	uint8_t err = sd_receive_r3_r7(response);
	sd_debug("response %02x\n", err);
	if ((err & SD_R1_OK_MASK) != 0) {
		return err;
	}
	sd_debug("ocr %02x %02x %02x %02x\n", response[0], response[1], response[2], response[3]);
	// TODO replace uint32_t with union+struct
	*ocr = (uint32_t) response[3] | ((uint32_t) response[2] << 8) | ((uint32_t) response[1] << 16) | ((uint32_t) response[0] << 24);
	return err;
}

// CMD59: enable/disable crc checking
uint8_t sd_send_cmd59(bool crc) {
	sd_cmd_t cmd59 = { { 59, 0x00, 0x00, 0x00, crc ? SD_CMD59_CRC_ON : 0x00, 0x00 } };
	sd_send_cmd(&cmd59, false);
	uint8_t err = sd_receive_r1_r1b();
	sd_debug("response %02x\n", err);
	return err;
}

// ACMD41: set capacity mode
// returns SD_R1_IDLE while card is initializing.
// returns SD_GENERAL_OK when initialization completes and the card is ready
// returns any other R1 response on error
uint8_t sd_send_acmd41(bool hcs) {
	// voltage settings aren't supported in SPI mode, so we don't set them
	sd_cmd_t cmd41 = { { 41, (hcs ? SD_ACMD41_HCS : 0x00), 0x00, 0x00, 0x00, 0x00 } };
	uint8_t err = sd_send_acmd(&cmd41, false);
	if ((err & SD_R1_OK_MASK) != 0) {
		// cancel if CMD55 fails
		return err;
	}
	// read the ACMD response
	err = sd_receive_r1_r1b();
	sd_debug("response %02x\n", err);
	return err;
}

// initialize SPI control lines (chip select and card detection)
void sd_init() {
	// actually, this is all done by gpio_init and spi_init, nothing else to do here.
}

// check if a card is inserted
bool sd_inserted() {
	// this pin is active low, invert the state
	return gpio_sddet_get();
}

// assert chip select
void sd_select() {
	// assert SDCS to enable communication with the SD card
	gpio_sdcs_set();
	// enable the output drivers on the level shifter
	gpio_sdoe_set();
	// level shifter enable delay
	_delay_us(SD_TCB0104_ENABLE_DELAY_US);
	// enable SPI in high speed mode
	spi_init(SD_DATA_CLOCK_DIV, SPI_MODE0, false);
	// we could start sending commands right away, but it's safer to clock in
	// one dummy transfer first
	spi_transfer(SD_DUMMY_HIGH);
	sd_state.asserted = true;
}

// de-assert chip select
void sd_deselect() {
	// we could start deassert right aways, but it's safer to clock in
	// one dummy transfer first
	spi_transfer(SD_DUMMY_HIGH);
	// clear chip select
	gpio_sdcs_clear();
	// disable the output drivers on the level shifter
	gpio_sdoe_clear();
	// level shifter disable delay
	_delay_us(SD_TCB0104_DISABLE_DELAY_US);
	sd_state.asserted = false;
}

// set up SPI for SD card initialization, assert chip select and run startup sequence
// it doesn't deassert chip select or output enable, so don't forget to do that afterwards
// if the bus is needed for something else.
uint8_t sd_start() {
	// reset state
	sd_state.initialized = false;
	sd_state.read_timeout = SD_TIMEOUT_BLOCK_READ_DEFAULT;

	// configure the SPI clock generator for 250kHz
	spi_init(SD_SETUP_CLOCK_DIV, SPI_MODE0, false);

	// ensure CS is deasserted for start up
	gpio_sdcs_clear();
	// enable the output drivers on the level shifter
	gpio_sdoe_set();
	// level shifter enable delay
	_delay_us(SD_TCB0104_ENABLE_DELAY_US);

	// keep CS deasserted and send 80 clocks to make sure the SD card is initialized
	// (required are 74, but it's easier to just do 10*8)
	gpio_sdcs_clear();
	for (uint8_t i = 0; i < SD_INIT_DUMMY_TRANSFERS; i++) {
		// dummy data = logic high, ignore response
		spi_transfer(SD_DUMMY_HIGH);
	}

	// assert CS now, so commands can be sent
	gpio_sdcs_set();
	// send two dummy transfers before starting
	spi_transfer(SD_DUMMY_HIGH);
	spi_transfer(SD_DUMMY_HIGH);

	// send CMD0 to return to idle
	uint8_t err = sd_send_cmd0();
	if ((err & SD_R1_OK_MASK) != 0) {
		// deassert and return error
		sd_debug("error init cmd0\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}

	// start with v2 card type
	uint8_t type = SD_CARD_TYPE_V2;

	// send CMD8 to ensure correct base voltage
	err = sd_send_cmd8();
	if ((err & SD_R1_OK_MASK) != 0) {
		if ((err & SD_R1_ILLEGAL_CMD) != 0) {
			// V1 memory card, usable
			type = SD_CARD_TYPE_V1;
		} else {
			// unusable card, deassert and return error
			sd_debug("error init cmd8\n");
			sd_deselect();
			return SD_GENERAL_ERROR;
		}
	}

	// check the voltages
	uint32_t ocr = 0;
	err = sd_send_cmd58(&ocr);
	if ((err & SD_R1_OK_MASK) != 0) {
		// deassert and return error
		sd_debug("error init cmd58\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}
	sd_debug("ocr 0x%08lx\n", ocr);
	if ((ocr & SD_CMD58_VDD_30_31) == 0) {
		sd_debug("error card doesn't support 3.0v\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}
	// the CCS bit is only checked at the end, after init

	// send ACMD41 to set up physical interface
	// and wait for completion by repeating the command and checking for 0
	uint8_t init;
	for (init = 0; init < SD_TIMEOUT_CARD_INIT; init++) {
		// we always enable HCS mode, the CCS register will indicate later
		// if it was enabled or not
		err = sd_send_acmd41(true);
		// the card returns 0 when it's done initializing,
		// so we're checking explicitly for that here
		// if the IDLE bit is set, the card is still initializing and we have
		// to wait and repeat
		if (err == SD_GENERAL_OK) {
			break;
		} else if (err != SD_R1_IDLE) {
			// deassert and return error
			sd_debug("error init acmd41\n");
			sd_deselect();
			return SD_GENERAL_ERROR;
		}
	}

	// check for a timeout
	if (init >= SD_TIMEOUT_CARD_INIT) {
		// deassert and return error
		sd_debug("error init timeout\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}

	// check card capacity
	ocr = 0;
	err = sd_send_cmd58(&ocr);
	if ((err & SD_R1_OK_MASK) != 0) {
		// deassert and return error
		sd_debug("error init cmd58\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}
	if ((ocr & SD_CMD58_CCS) != 0) {
		sd_debug("high capacity card\n");
		type = SD_CARD_TYPE_V2_HCS;
	}

	if (type != SD_CARD_TYPE_V2_HCS) {
		// on standard capacity cards, we need to make sure that the block size
		// is set to 512, so it behaves like a HCS card
		err = sd_send_cmd16(512);
		if ((err & SD_R1_OK_MASK) != 0) {
			// deassert and return error
			sd_debug("error init cmd16\n");
			sd_deselect();
			return SD_GENERAL_ERROR;
		}
	}

	// TODO read CSD and CID registers
	// TODO assign the real value to sd_state.read_timeout

	// enable CRC checking
	/*err = sd_send_cmd59(true);
	if ((err & SD_R1_OK_MASK) != 0) {
		// deassert and return error
		sd_debug("error init cmd59\n");
		sd_deselect();
		return SD_GENERAL_ERROR;
	}*/

	// switch to high speed mode
	spi_init(SD_DATA_CLOCK_DIV, SPI_MODE0, false);

	sd_state.initialized = true;
	sd_state.asserted = true;
	sd_state.card_type = type;

	// keep CS asserted on success
	return type;
}

// check if the currently initialized card is HCS (high capacity)
static inline bool sd_is_hcs() {
	return sd_state.card_type == SD_CARD_TYPE_V2_HCS;
}

// read one block of data from the card
uint8_t sd_read(uint32_t blockid, uint8_t data[512]) {
	if (data == NULL) {
		sd_debug("buffer is NULL\n");
		return SD_GENERAL_ERROR;
	}
	if (!sd_state.initialized) {
		sd_debug("card initialized\n");
		return SD_GENERAL_ERROR;
	}
	if (!sd_state.asserted) {
		sd_debug("card chip select not asserted\n");
		return SD_GENERAL_ERROR;
	}

	sd_debug("reading block #0x%08x\n", blockid);
	// standard capacity cards require a byte address instead of a block id
	if (!sd_is_hcs()) {
		// make sure the 32bit block address won't be exceeded
		if (blockid >= SD_MAX_BLOCK_ADDRESS_SC) {
			sd_debug("block address exceeded\n");
			return SD_GENERAL_ERROR;
		}
		blockid *= SD_BLOCK_SIZE;
	}
	
	// request a block
	uint8_t err = sd_send_cmd17(blockid);
	if ((err & SD_R1_OK_MASK) != 0) {
		sd_debug("error cmd17\n");
		return SD_GENERAL_ERROR;
	}

	// wait for start token
	// TODO max wait time is specified in the CSD register
	// TODO we could also reduce bus noise and wait a bit before checking again
	uint16_t ac;
	for (ac = 0; ac < sd_state.read_timeout; ac++) {
		uint8_t token = spi_transfer(SD_DUMMY_HIGH);
		if (token == SD_SINGLE_START_TOKEN) {
			break;
		}
	}
	// check for timeout
	if (ac >= sd_state.read_timeout) {
		sd_debug("timeout waiting for start\n");
		// TODO send stop transmission token
		return SD_GENERAL_ERROR;
	}

	// read one block of data
	for (uint16_t i = 0; i < SD_BLOCK_SIZE; i++) {
		data[i] = spi_transfer(SD_DUMMY_HIGH);
	}

	// read CRC
	uint16_t crc = ((uint16_t) spi_transfer(SD_DUMMY_HIGH) << 8) | spi_transfer(SD_DUMMY_HIGH);
	// calculate and compare CRC
	uint16_t dcrc = sd_crc16(data, 512);
	if (crc != dcrc) {
		sd_debug("mismatched CRC %04x, expected %04x\n", crc, dcrc);
		// TODO send stop transmission token
		return SD_GENERAL_ERROR;
	}

	// probably needed
	spi_transfer(SD_DUMMY_HIGH);

	return SD_GENERAL_OK;
}
