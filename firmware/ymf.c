/*
 *  Copyright (C) 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>
#include "ymf.h"
#include "gpio.h"
#include "spi.h"

// all in µs
#define FM_T_CSW 0.100
#define FM_T_WDH 0.020
#define FM_T_CSR 0.150
#define FM_T_RDH 0.010
// minimum wait time between all write operations
#define FM_T_AD (32 * 1000000.0 / F_OPL)

// enabling the register cache requires 512 bytes of RAM
#ifdef YMF_ENABLE_CACHE
__attribute__((section (".noinit")))
static uint8_t ymf_cache[2][256];
#endif //YMF_ENABLE_CACHE

#if BOARD_REV >= 5
// v5 and newer expose the data bus directly
void ymf_begin() {
	gpio_ycs_clear();
	gpio_a0_low();
	gpio_a1_low();
	gpio_data_set(0x00);
}

void ymf_end() {
	gpio_ycs_clear();
	gpio_a0_low();
	gpio_a1_low();
	gpio_data_set(0x00);
}

static void ymf_write(uint8_t data) {
	gpio_rdwr_write();
	gpio_data_set(data);
	gpio_ycs_set();
	_delay_us(FM_T_CSW);
	gpio_ycs_clear();
	_delay_us(FM_T_WDH);
	_delay_us(FM_T_AD);
}

static uint8_t ymf_read() {
	gpio_data_in();
	gpio_rdwr_read();
	gpio_ycs_set();
	_delay_us(FM_T_CSR);
	uint8_t data = gpio_data_get();
	gpio_ycs_clear();
	_delay_us(FM_T_RDH);
	gpio_rdwr_write();
	gpio_data_out();
	return data;
}
#elif BOARD_REV >= 3 && BOARD_REV < 5
// v3 and v4 use a shift register for the data bus
void ymf_begin() {
	gpio_ycs_clear();
	gpio_yoe_clear();
	gpio_a0_low();
	gpio_a1_low();
	spi_init(SPI_CLK_DIV_2, SPI_MODE0, false);
}

void ymf_end() {
	gpio_ycs_clear();
	gpio_yoe_clear();
	gpio_a0_low();
	gpio_a1_low();
	spi_disable();
}

static void ymf_write(uint8_t data) {
	gpio_rdwr_write();
	spi_transfer(data);
	gpio_yoe_set();
	gpio_ycs_set();
	_delay_us(FM_T_CSW);
	gpio_ycs_clear();
	gpio_yoe_clear();
	_delay_us(FM_T_WDH);
}

static uint8_t ymf_read() {
	gpio_rdwr_read();
	gpio_ycs_set();
	_delay_us(FM_T_CSR);
	// send one clock cycle to shift reg to latch the data
	// toggling SCK seems to be disabled when SPI is active
	// so we quickly disable SPI, then reenable it
	SPCR &= ~_BV(SPE);
	gpio_sck_set();
	gpio_sck_clear();
	SPCR |= _BV(SPE);
	gpio_ycs_clear();
	_delay_us(FM_T_RDH);
	gpio_rdwr_write();
	// read out data from shift register (OPL is already off the bus)
	uint8_t data = spi_transfer(0);
	return data;
}
#else
// other versions aren't supported
#warning unsupported board revision, YMF is not accessible
#endif

uint8_t ymf_read_status() {
	gpio_a0_low();
	gpio_a1_low();
	uint8_t status = ymf_read();
	return status;
}

void ymf_write_reg(uint8_t bank, uint8_t reg, uint8_t data) {
	if (bank) {
		gpio_a1_high();
	} else {
		gpio_a1_low();
	}
	gpio_a0_low();
	ymf_write(reg);
	// not really necessary (data sheet says X: Don't care)
	gpio_a1_low();
	gpio_a0_high();
	ymf_write(data);
#ifdef YMF_ENABLE_CACHE
	ymf_cache[bank ? 1 : 0][reg] = data;
#endif //YMF_ENABLE_CACHE
}

uint8_t ymf_detect() {
	// Courtesy of: https://www.fit.vutbr.cz/~arnost/opl/opl3.html#appendixB
	// OPL2 detection
	// Reset Timer 1 and Timer 2: write 60h to register 4.
	ymf_write_reg(0, 0x04, 0x60);
	// Reset the IRQ: write 80h to register 4.
	ymf_write_reg(0, 0x04, 0x80);
	// Note: Steps 1 and 2 can't be combined together.
	// Read status register: read port base+0 (388h). Save the result.
	uint8_t status = ymf_read_status();
	// Set Timer 1 to FFh: write FFh to register 2.
	ymf_write_reg(0, 0x02, 0xff);
	// Unmask and start Timer 1: write 21h to register 4.
	ymf_write_reg(0, 0x04, 0x21);
	// Wait in a delay loop for at least 80 usec. (100 to be safe)
	_delay_us(100);
	// Read status register: read port base+0 (388h). Save the result.
	uint8_t status2 = ymf_read_status();
	// Reset Timer 1, Timer 2 and IRQ as in steps 1 and 2.
	ymf_write_reg(0, 0x04, 0x60);
	ymf_write_reg(0, 0x04, 0x80);
	// Test the results of the two reads: the first should be 0, the second should be C0h.
	// If either is incorrect, then the OPL2 is not present.
	uint8_t result = YMF_DETECT_NONE;
	if ((status & 0xe0) == 0 && (status2 & 0xe0) == 0xc0) {
		// OPL3 detection (note: this is not an official method and may not produce correct results!)
		// Read status register: read port base+0.
		uint8_t status3 = ymf_read_status();
		// AND the result with 06h.
		// If the result is zero, you have OPL3, otherwise OPL2.
		if ((status3 & 0x06) == 0) {
			result = YMF_DETECT_OPL3;
		} else {
			result = YMF_DETECT_OPL2;
		}
	}
	return result;
}

#ifdef YMF_ENABLE_CACHE

void ymf_init() {
	memset(ymf_cache, 0, sizeof(ymf_cache));
}

uint8_t ymf_read_reg(uint8_t bank, uint8_t reg) {
	return ymf_cache[bank ? 1 : 0][reg];
}

#else //YMF_ENABLE_CACHE

void ymf_init() {
	// nop
}

uint8_t ymf_read_reg(uint8_t bank, uint8_t reg) {
	return 0;
}

#endif //YMF_ENABLE_CACHE
