/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stddef.h>
#include "fat_debug.h"
#include "fat_private.h"

void fat_debug_mbr(fat_mbr_t *fat_mbr) {
#if !__AVR__
	for (uint8_t i = 0; i < 4; i++) {
		printf("Partition: %u ", i);
		printf("Bootable: %c ", fat_mbr->partition[i].status.boot ? 'y' : 'n');
		printf("Type: 0x%02x ", fat_mbr->partition[i].type);
		printf("First CHS: %u:%u:%u ", ((uint16_t) fat_mbr->partition[i].first.cylinderh << 8) | fat_mbr->partition[i].first.cylinderl, fat_mbr->partition[i].first.head, fat_mbr->partition[i].first.sector);
		printf("Last CHS: %u:%u:%u ", ((uint16_t) fat_mbr->partition[i].last.cylinderh << 8) | fat_mbr->partition[i].last.cylinderl, fat_mbr->partition[i].last.head, fat_mbr->partition[i].last.sector);
		printf("First LBA: %u ", fat_mbr->partition[i].lbafirst);
		printf("Sectors: %u ", fat_mbr->partition[i].sectors);
		printf("\n");
	}
	printf("Boot signature: 0x%02x 0x%02x\n", fat_mbr->bootsignature[0], fat_mbr->bootsignature[1]);
#endif
}

void fat_debug_boot_sector(fat_boot_sector_t *fat_main_boot) {
#if !__AVR__
	printf("FileSystemName: %8s ", fat_main_boot->FileSystemName);
	printf("PartitionOffset: %lu ", fat_main_boot->PartitionOffset);
	printf("VolumeLength: %lu ", fat_main_boot->VolumeLength);
	printf("FatOffset: %u ", fat_main_boot->FatOffset);
	printf("FatLength: %u ", fat_main_boot->FatLength);
	printf("ClusterHeapOffset: %u ", fat_main_boot->ClusterHeapOffset);
	printf("ClusterCount: %u ", fat_main_boot->ClusterCount);
	printf("FirstClusterOfRootDirectory: %u ", fat_main_boot->FirstClusterOfRootDirectory);
	printf("VolumeSerialNumber: %u ", fat_main_boot->VolumeSerialNumber);
	printf("FileSystemRevision: %u.%u ", fat_main_boot->FileSystemRevision.High, fat_main_boot->FileSystemRevision.Low);
	printf("VolumeFlags.ActiveFat: %c ", fat_main_boot->VolumeFlags.ActiveFat ? 'y' : 'n');
	printf("VolumeFlags.VolumeDirty: %c ", fat_main_boot->VolumeFlags.VolumeDirty ? 'y' : 'n');
	printf("VolumeFlags.MediaFailure: %c ", fat_main_boot->VolumeFlags.MediaFailure ? 'y' : 'n');
	printf("VolumeFlags.ClearToZero: %c ", fat_main_boot->VolumeFlags.ClearToZero ? 'y' : 'n');
	printf("BytesPerSectorShift: %u ", fat_main_boot->BytesPerSectorShift);
	printf("SectorsPerClusterShift: %u ", fat_main_boot->SectorsPerClusterShift);
	printf("NumberOfFats: %u ", fat_main_boot->NumberOfFats);
	printf("DriveSelect: %u ", fat_main_boot->DriveSelect);
	printf("PercentInUse: %u%% ", fat_main_boot->PercentInUse);
	printf("BootSignature: %02x ", fat_main_boot->BootSignature);
	printf("\n");
#endif
}

void fat_debug_chain(fat_entries_t *fat_entries) {
#if !__AVR__
	for (uint32_t i = 0; i < FAT_ENTRIES_PER_BLOCK; i++) {
		printf("[%u]0x%08x ", i, fat_entries->FatEntry[i]);
	}
	printf("\n");
#endif
}

void fat_debug_dir_entry(fat_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: DirEntry ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("FirstCluster: %u ", entry->FirstCluster);
	printf("DataLength: %lu ", entry->DataLength);
	printf("\n");
#endif
}

void fat_debug_volume_label(fat_volume_label_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: VolumeLabel ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	wchar_t wlabel[11];
	for (uint8_t j = 0; j < 11; j++) {
		wlabel[j] = entry->VolumeLabel[j];
	}
	printf("VolumeLabel: %*ls ", entry->CharacterCount, wlabel);
	printf("\n");
#endif
}

void fat_debug_file_entry(fat_file_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: File ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("SecondaryCount: %u ", entry->SecondaryCount);
	printf("SetChecksum: 0x%02x ", entry->SetChecksum);
	printf("ReadOnly: %c ", entry->FileAttributes.ReadOnly ? 'y' : 'n');
	printf("Hidden: %c ", entry->FileAttributes.Hidden ? 'y' : 'n');
	printf("System: %c ", entry->FileAttributes.System ? 'y' : 'n');
	printf("Directory: %c ", entry->FileAttributes.Directory ? 'y' : 'n');
	printf("Archive: %c ", entry->FileAttributes.Archive ? 'y' : 'n');
	printf("\n");
#endif
}

void fat_debug_allocation_bitmap(fat_allocation_bitmap_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: AllocationBitmap ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("BitmapIdentifier: %u ", entry->BitmapFlags.BitmapIdentifier);
	printf("FirstCluster: %u ", entry->FirstCluster);
	printf("DataLength: %lu ", entry->DataLength);
	printf("\n");
#endif
}

void fat_debug_upcase_table(fat_upcase_table_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: UpcaseTable ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("TableChecksum: 0x%04x ", entry->TableChecksum);
	printf("FirstCluster: %u ", entry->FirstCluster);
	printf("DataLength: %lu ", entry->DataLength);
	printf("\n");
#endif
}

void fat_debug_stream_extension(fat_stream_extension_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: StreamExtension ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("AllocationPossible: %c ", entry->GeneralSecondaryFlags.AllocationPossible ? 'y' : 'n');
	printf("NoFatChain: %c ", entry->GeneralSecondaryFlags.NoFatChain ? 'y' : 'n');
	printf("NameLength: %u ", entry->NameLength);
	printf("NameHash: 0x%04x ", entry->NameHash);
	printf("ValidDataLength: %lu ", entry->ValidDataLength);
	printf("FirstCluster: %u ", entry->FirstCluster);
	printf("DataLength: %lu ", entry->DataLength);
	printf("\n");
#endif
}

void fat_debug_file_name(fat_file_name_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: FileName ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("AllocationPossible: %c ", entry->GeneralSecondaryFlags.AllocationPossible ? 'y' : 'n');
	printf("NoFatChain: %c ", entry->GeneralSecondaryFlags.NoFatChain ? 'y' : 'n');
	wchar_t wname[16];
	for (uint8_t j = 0; j < 15; j++) {
		wname[j] = entry->FileName[j];
	}
	wname[15] = 0;
	printf("FileName: %ls ", wname);
	printf("\n");
#endif
}

void fat_debug_volume_guid(fat_volume_guid_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: VolumeGuid ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("AllocationPossible: %c ", entry->GeneralPrimaryFlags.AllocationPossible ? 'y' : 'n');
	printf("NoFatChain: %c ", entry->GeneralPrimaryFlags.NoFatChain ? 'y' : 'n');
	printf("VolumeGuid: %08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x ", entry->VolumeGuid.Data1, entry->VolumeGuid.Data2, entry->VolumeGuid.Data3, entry->VolumeGuid.Data4[0], entry->VolumeGuid.Data4[1], entry->VolumeGuid.Data5[0], entry->VolumeGuid.Data5[1], entry->VolumeGuid.Data5[2], entry->VolumeGuid.Data5[3], entry->VolumeGuid.Data5[4], entry->VolumeGuid.Data5[5]);
	printf("\n");
#endif
}

void fat_debug_vendor_extension(fat_vendor_extension_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: VendorExtension ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("AllocationPossible: %c ", entry->GeneralSecondaryFlags.AllocationPossible ? 'y' : 'n');
	printf("NoFatChain: %c ", entry->GeneralSecondaryFlags.NoFatChain ? 'y' : 'n');
	printf("VendorGuid: %08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x ", entry->VendorGuid.Data1, entry->VendorGuid.Data2, entry->VendorGuid.Data3, entry->VendorGuid.Data4[0], entry->VendorGuid.Data4[1], entry->VendorGuid.Data5[0], entry->VendorGuid.Data5[1], entry->VendorGuid.Data5[2], entry->VendorGuid.Data5[3], entry->VendorGuid.Data5[4], entry->VendorGuid.Data5[5]);
	printf("VendorDefined: ");
	for (uint8_t i = 0; i < 14; i++) {
		printf("%02x ", entry->VendorDefined[i]);
	}
	printf("\n");
#endif
}

void fat_debug_vendor_allocation(fat_vendor_allocation_dir_entry_t *entry) {
#if !__AVR__
	printf("Type: VendorAllocation ");
	printf("TypeCode: 0x%02x ", entry->EntryType.TypeCode);
	printf("TypeImportance: %c ", entry->EntryType.TypeImportance ? 'y' : 'n');
	printf("TypeCategory: %c ", entry->EntryType.TypeCategory ? 'y' : 'n');
	printf("InUse: %c ", entry->EntryType.InUse ? 'y' : 'n');
	printf("AllocationPossible: %c ", entry->GeneralSecondaryFlags.AllocationPossible ? 'y' : 'n');
	printf("NoFatChain: %c ", entry->GeneralSecondaryFlags.NoFatChain ? 'y' : 'n');
	printf("VendorGuid: %08x-%04x-%04x-%02x%02x-%02x%02x%02x%02x%02x%02x ", entry->VendorGuid.Data1, entry->VendorGuid.Data2, entry->VendorGuid.Data3, entry->VendorGuid.Data4[0], entry->VendorGuid.Data4[1], entry->VendorGuid.Data5[0], entry->VendorGuid.Data5[1], entry->VendorGuid.Data5[2], entry->VendorGuid.Data5[3], entry->VendorGuid.Data5[4], entry->VendorGuid.Data5[5]);
	printf("VendorDefined: ");
	for (uint8_t i = 0; i < 2; i++) {
		printf("%02x ", entry->VendorDefined[i]);
	}
	printf("FirstCluster: %u ", entry->FirstCluster);
	printf("DataLength: %lu ", entry->DataLength);
	printf("\n");
#endif
}

void fat_debug_any_entry(fat_dir_entry_t *entry) {
	if (entry->EntryType.InUse == 1) {
		if (entry->EntryType.TypeImportance == 0) {
			if (entry->EntryType.TypeCategory == 0) {
				// critical primary
				switch (entry->EntryType.TypeCode) {
					case FAT_DIR_ENTRY_CODE_ALLOCATION_BITMAP: {
						fat_allocation_bitmap_dir_entry_t *allocentry = (fat_allocation_bitmap_dir_entry_t *) entry;
						fat_debug_allocation_bitmap(allocentry);
					} break;
					case FAT_DIR_ENTRY_CODE_FILE: {
						fat_file_dir_entry_t *fileentry = (fat_file_dir_entry_t *) entry;
						fat_debug_file_entry(fileentry);
					} break;
					case FAT_DIR_ENTRY_CODE_UPCASE_TABLE: {
						fat_upcase_table_dir_entry_t *upcaseentry = (fat_upcase_table_dir_entry_t *) entry;
						fat_debug_upcase_table(upcaseentry);
					} break;
					case FAT_DIR_ENTRY_CODE_VOLUME_LABEL: {
						fat_volume_label_dir_entry_t *labelentry = (fat_volume_label_dir_entry_t *) entry;
						fat_debug_volume_label(labelentry);
					} break;
					default:
						// debugging
						fat_debug_dir_entry(entry);
				}
			} else {
				switch (entry->EntryType.TypeCode) {
					case FAT_DIR_ENTRY_CODE_STREAM_EXTENSION: {
						fat_stream_extension_dir_entry_t *allocentry = (fat_stream_extension_dir_entry_t *) entry;
						fat_debug_stream_extension(allocentry);
					} break;
					case FAT_DIR_ENTRY_CODE_FILE_NAME: {
						fat_file_name_dir_entry_t *allocentry = (fat_file_name_dir_entry_t *) entry;
						fat_debug_file_name(allocentry);
					} break;
					default:
						// debugging
						fat_debug_dir_entry(entry);
				}
			}
		} else {
			if (entry->EntryType.TypeCategory == 0) {
				// benign primary
				switch (entry->EntryType.TypeCode) {
					case FAT_DIR_ENTRY_CODE_VOLUME_GUID: {
						fat_volume_guid_dir_entry_t *allocentry = (fat_volume_guid_dir_entry_t *) entry;
						fat_debug_volume_guid(allocentry);
					} break;
					// unsupported
					//case FAT_DIR_ENTRY_CODE_TEXFAT_PADDING:
					//	break;
					default:
						// debugging
						fat_debug_dir_entry(entry);
				}
			} else {
				// benign secondary
				switch (entry->EntryType.TypeCode) {
					case FAT_DIR_ENTRY_CODE_VENDOR_EXTENSION: {
						fat_vendor_extension_dir_entry_t *allocentry = (fat_vendor_extension_dir_entry_t *) entry;
						fat_debug_vendor_extension(allocentry);
					} break;
					case FAT_DIR_ENTRY_CODE_VENDOR_ALLOCATION: {
						fat_vendor_allocation_dir_entry_t *allocentry = (fat_vendor_allocation_dir_entry_t *) entry;
						fat_debug_vendor_allocation(allocentry);
					} break;
					default:
						// debugging
						fat_debug_dir_entry(entry);
				}
			}
		}
	}
}

fat_error_t fat_debug_root_directory() {
	fat_error_t err = FAT_OK;
	for (uint8_t i = 0; i < FAT_DIR_ENTRIES_PER_BLOCK; i++) {
		fat_dir_entry_t *entry = NULL;
		// the root cluster seems to be always chained (no NoFatChain flag in boot sector)
		// but other dir entries can be chained or not, depending on the flag in their referencing secondary dir entry
		err = fat_load_directory_entry(fat_root_cluster, i, &entry, false);
		if (err != FAT_OK) return err;
		if (entry->EntryType.TypeCode == FAT_DIR_ENTRY_CODE_END && entry->EntryType.InUse == 0 && entry->EntryType.TypeImportance == 0 && entry->EntryType.TypeCategory == 0) break;
		fat_debug_any_entry(entry);
	}
	return err;
}
