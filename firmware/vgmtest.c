/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>

#define F_CPU 16000000
#define CLK_DIVISOR 64
#define CLK_DT (1.0 * (CLK_DIVISOR) / (F_CPU))
#define CLK_STEPS_US(timestep) ((uint32_t) ((timestep) / (1000000.0 * (CLK_DT))))

#define VGM_MODE_OPL1 1
#define VGM_MODE_OPL2 2
#define VGM_MODE_OPL3 3
#define VGM_MODE_OPL4 4
#define VGM_MODE_OPL_MSX 25

typedef struct {
	uint32_t version;
	uint32_t data_length;
	uint32_t samples;
	uint32_t loop_offset;
	uint32_t loop_samples;
	uint32_t data_offset;
	uint32_t opl_clock;
	uint8_t opl_mode;
	uint8_t volume_modifier;
	uint8_t loop_base;
	uint8_t loop_modifier;
} vgm_header_t;

static const char vgm_magic[] = "Vgm ";
#define VGM_SAMPLE_RATE 44100
#define VGM_OPL_CLOCK 14318180
#define VGM_MIN_VERSION 0x151
#define VGM_CPU_RATE ((uint32_t) (F_CPU / VGM_SAMPLE_RATE))
#define VGM_SAMPLES_TO_US(samples) ((uint32_t) ((samples) * 1000000.0 / (VGM_SAMPLE_RATE)))

static bool read_bytes(FILE *fp, uint8_t *buf, uint8_t len) {
	if (!fp || !buf) {
		return false;
	}
	if (fread(buf, 1, len, fp) != len) {
		return false;
	}
	return true;
}

static bool skip_bytes(FILE *fp, uint8_t len) {
	if (fseek(fp, len, SEEK_CUR) == -1) {
		return false;
	}
	return true;
}

static uint32_t bytes_to_u32le(uint8_t buf[4]) {
	return buf[0] | (buf[1] << 8) | (buf[2] << 16) | (buf[3] << 24);
}

static uint16_t bytes_to_u16le(uint8_t buf[2]) {
	return buf[0] | (buf[1] << 8);
}

void ymf_write_reg(uint8_t bank, uint8_t reg, uint8_t data) {
	printf("Y[%u,%02X]=%02X\n", bank, reg, data);
}

static void vgm_wait(uint16_t samples) {
	// TODO optimize calculation
	// TODO issue multiple wait commands when range exceeded (65535*DT = 262.14ms; 65535*DT*SAMPLE_RATE = 11560 samples)
	printf("W%u|%uus|%uT\n", samples, VGM_SAMPLES_TO_US(samples), CLK_STEPS_US(VGM_SAMPLES_TO_US(samples)));
}

uint8_t vgm_read_header(FILE *fp, vgm_header_t *hdr) {
	if (!fp || !hdr) {
		return 99;
	}
	uint8_t buf[4];
	// "Vgm " ident
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	for (uint8_t i = 0; i < sizeof(buf); i++) {
		if (buf[i] != (uint8_t) vgm_magic[i]) {
			return 2;
		}
	}
	// EoF offset
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->data_length = bytes_to_u32le(buf);
	// Version
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->version = bytes_to_u32le(buf);
	if (hdr->version < VGM_MIN_VERSION) {
		return 3;
	}
	if (!skip_bytes(fp, 12)) {
		return 1;
	}
	// Total # samples
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->samples = bytes_to_u32le(buf);
	// Loop offset
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->loop_offset = bytes_to_u32le(buf);
	// Loop # samples
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->loop_samples = bytes_to_u32le(buf);
	if (!skip_bytes(fp, 16)) {
		return 1;
	}
	// VGM data offset
	if (!read_bytes(fp, buf, sizeof(buf))) {
		return 1;
	}
	hdr->data_offset = bytes_to_u32le(buf);
	if (hdr->data_offset >= 28) {
		if (!skip_bytes(fp, 24)) {
			return 1;
		}
	}
	uint32_t opl2_clock = 0;
	if (hdr->data_offset >= 32) {
		// YM3812 clock
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		opl2_clock = bytes_to_u32le(buf);
	}
	uint32_t opl1_clock = 0;
	if (hdr->data_offset >= 36) {
		// YM3526 clock
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		opl1_clock = bytes_to_u32le(buf);
	}
	uint32_t opl_msx_clock = 0;
	if (hdr->data_offset >= 40) {
		// Y8950 clock
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		opl_msx_clock = bytes_to_u32le(buf);
	}
	uint32_t opl3_clock = 0;
	if (hdr->data_offset >= 44) {
		// YMF262 clock
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		opl3_clock = bytes_to_u32le(buf);
	}
	uint32_t opl4_clock = 0;
	if (hdr->data_offset >= 48) {
		// YMF278B clock
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		opl4_clock = bytes_to_u32le(buf);
	}
	if (hdr->data_offset >= 64) {
		if (!skip_bytes(fp, 12)) {
			return 1;
		}
		// VM || *** || LB || LM
		if (!read_bytes(fp, buf, sizeof(buf))) {
			return 1;
		}
		hdr->volume_modifier = buf[0];
		hdr->loop_base = buf[2];
		hdr->loop_modifier = buf[3];
	} else {
		hdr->volume_modifier = 0;
		hdr->loop_base = 0;
		hdr->loop_modifier = 0;
	}
	if (hdr->data_offset > 64) {
		printf("Skip %u bytes from %lu\n", hdr->data_offset - 64, ftell(fp));
		if (!skip_bytes(fp, hdr->data_offset - 64)) {
			return 1;
		}
	}

	if (opl4_clock) {
		hdr->opl_mode = VGM_MODE_OPL4;
		hdr->opl_clock = opl4_clock;
	} else if (opl3_clock) {
		hdr->opl_mode = VGM_MODE_OPL3;
		hdr->opl_clock = opl3_clock;
	} else if (opl_msx_clock) {
		hdr->opl_mode = VGM_MODE_OPL_MSX;
		hdr->opl_clock = opl_msx_clock;
	} else if (opl2_clock) {
		hdr->opl_mode = VGM_MODE_OPL2;
		hdr->opl_clock = opl2_clock;
	} else if (opl1_clock) {
		hdr->opl_mode = VGM_MODE_OPL1;
		hdr->opl_clock = opl1_clock;
	} else {
		return 4;
	}

	return 0;
}

bool vgm_next(vgm_header_t *hdr, FILE *fp) {
	uint8_t cmd, bank, reg, data;
	uint8_t buf[2];
	if (fread(&cmd, 1, sizeof(cmd), fp) != sizeof(cmd)) {
		return false;
	}
	switch (cmd) {
		case 0x5A:
			// aa dd YM3812, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL2 && hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL_MSX && hdr->opl_mode != VGM_MODE_OPL4) {
				printf("!>=OPL2\n");
			}
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5B:
			// aa dd YM3526, write value dd to register aa
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5C:
			// aa dd Y8950, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL_MSX) {
				printf("!=MSX\n");
			}
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5E:
			// aa dd YMF262 port 0, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL4) {
				printf("!>=OPL3\n");
			}
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5F:
			// aa dd YMF262 port 1, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL4) {
				printf("!>=OPL3\n");
			}
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(1, reg, data);
			break;
		case 0x61:
			// nn nn Wait n samples, n can range from 0 to 65535 (approx 1.49 seconds). Longer pauses than this are represented by multiple wait commands.
			if (fread(&buf, 1, sizeof(buf), fp) != sizeof(buf)) {
				return false;
			}
			vgm_wait(bytes_to_u16le(buf));
			break;
		case 0x62:
			// wait 735 samples (60th of a second), a shortcut for 0x61 0xdf 0x02
			vgm_wait(735);
			break;
		case 0x63:
			// wait 882 samples (50th of a second), a shortcut for 0x61 0x72 0x03
			vgm_wait(882);
			break;
		case 0x66:
			// end of sound data
			return false;
		case 0x70:
		case 0x71:
		case 0x72:
		case 0x73:
		case 0x74:
		case 0x75:
		case 0x76:
		case 0x77:
		case 0x78:
		case 0x79:
		case 0x7a:
		case 0x7b:
		case 0x7c:
		case 0x7d:
		case 0x7e:
		case 0x7f:
			// wait n+1 samples, n can range from 0 to 15.
			vgm_wait(cmd - 0x70);
			break;
		case 0xD0:
			// pp aa dd YMF278B, port pp, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL4) {
				printf("!=OPL4\n");
			}
			if (fread(&bank, 1, sizeof(bank), fp) != sizeof(bank)) {
				return false;
			}
			if (fread(&reg, 1, sizeof(reg), fp) != sizeof(reg)) {
				return false;
			}
			if (fread(&data, 1, sizeof(data), fp) != sizeof(data)) {
				return false;
			}
			ymf_write_reg(bank, reg, data);
			break;
		default:
			printf("?%02X\n", cmd);
			return false;
	}
	return true;
}

int main(int argc, const char *argv[]) {
	if (argc < 2) {
		printf("Usage: vgmtest [file.vgm]\n");
		exit(-1);
	}

	FILE *fp = NULL;
	fp = fopen(argv[1], "rb");
	if (!fp) {
		perror("Can't open input file");
		exit(-1);
	}

	vgm_header_t hdr;
	uint8_t res = vgm_read_header(fp, &hdr);
	if (res != 0) {
		printf("Invalid VGM file: %u\n", res);
		exit(-1);
	}
	printf("version=%u.%u%u\n", (hdr.version >> 8) & 0xf, (hdr.version >> 4) & 0xf, (hdr.version >> 0) & 0xf);
	printf("data_length=%u\n", hdr.data_length);
	printf("samples=%u\n", hdr.samples);
	printf("loop_offset=%u\n", hdr.loop_offset);
	printf("loop_samples=%u\n", hdr.loop_samples);
	printf("data_offset=%u\n", hdr.data_offset);
	printf("opl_clock=%u\n", hdr.opl_clock);
	printf("opl_mode=%u\n", hdr.opl_mode);
	printf("volume_modifier=%u\n", hdr.volume_modifier);
	printf("loop_base=%u\n", hdr.loop_base);
	printf("loop_modifier=%u\n", hdr.loop_modifier);
	printf("Song length: %us\n", hdr.samples / hdr.opl_clock);
	if (hdr.opl_clock != VGM_OPL_CLOCK) {
		printf("OPL clock is not the expected %uHz. Got: %uHz. Song pitch may be out of tune.\n", VGM_OPL_CLOCK, hdr.opl_clock);
	}
	if (hdr.loop_offset || hdr.loop_samples || hdr.loop_base || hdr.loop_modifier) {
		printf("Loops aren't supported.\n");
	}
	if (hdr.volume_modifier) {
		printf("Volume modifiers aren't supported.\n");
	}

	printf("START\n");
	bool running = true;
	while (running) {
		running = vgm_next(&hdr, fp);
	}
	printf("END\n");

	fclose(fp);
}
