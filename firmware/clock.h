/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CLOCK_H
#define _CLOCK_H

#include <stdbool.h>

#define CLK_DT (1.0 * (CLK_DIVISOR) / (F_CPU))
#define CLK_STEPS_US(timestep) ((uint16_t) ((timestep) / (1000000.0 * (CLK_DT))))

void clock_init();
bool clock_elapsed(uint16_t timeout);
void clock_wait(uint16_t timeout);

#endif //_CLOCK_H
