/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <util/delay.h>
#include "spi.h"

void spi_init(uint8_t clock, uint8_t mode, bool lsb) {
	// SPI enable, master, MSB first
	SPCR = _BV(SPE) | _BV(MSTR) | (mode & 0x0c) | (lsb ? _BV(DORD) : 0x00);
	switch (clock) {
		case SPI_CLK_DIV_2:
			SPSR |= _BV(SPI2X);
			break;
		case SPI_CLK_DIV_4:
			break;
		case SPI_CLK_DIV_8:
			SPCR |= _BV(SPR0);
			SPSR |= _BV(SPI2X);
			break;
		case SPI_CLK_DIV_16:
			SPCR |= _BV(SPR0);
			break;
		case SPI_CLK_DIV_32:
			SPCR |= _BV(SPR1);
			SPSR |= _BV(SPI2X);
			break;
		case SPI_CLK_DIV_64:
			SPCR |= _BV(SPR1);
			break;
		case SPI_CLK_DIV_128:
			SPCR |= _BV(SPR0) | _BV(SPR1);
			break;
	}
}

void spi_disable() {
	SPCR &= ~(_BV(SPE) | _BV(MSTR));
}

uint8_t spi_transfer(uint8_t output) {
	SPDR = output;
	while ((SPSR & _BV(SPIF)) == 0);
	return SPDR;
}
