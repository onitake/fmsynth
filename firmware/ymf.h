/*
 *  Copyright (C) 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>

// TODO support YM3526 (OPL) in addition to OPL2 and OPL3

// Start interaction with the OPL:
// Set I/O lines to default state and enable SPI mode
// This function must be called at least once in your program, and at least
// once after calling ymf_end, before you can use ymf_write_reg,
// ymf_read_status or ymf_detect.
void ymf_begin();
// Restore all I/O pins to default state, ending interaction with the OPL
// This function should be called before using the SPI for communication with
// other devices.
void ymf_end();

// Write value "data" to register "reg" in memory bank "bank" of the OPL
// bank = 0..1 for YMF-262, 0 for YM3812
void ymf_write_reg(uint8_t bank, uint8_t reg, uint8_t data);
// Read status byte
uint8_t ymf_read_status();
// Detect OPL chip
// Returns:
// No OPL detected
#define YMF_DETECT_NONE 0
// OPL2 (YM-3812) found
#define YMF_DETECT_OPL2 2
// OPL3 (YMF-262) found
#define YMF_DETECT_OPL3 3
uint8_t ymf_detect();

#ifdef YMF_ENABLE_CACHE
#define YMF_WARN_NOCACHE(msg)
#else
#define YMF_WARN_NOCACHE(msg) __attribute__((warning(msg)))
#endif
// Clears the register cache.
// Bear in mind that this function does not reset the OPL in any way,
// so the register values may not correspond exactly with the hardware.
YMF_WARN_NOCACHE("function is a no-op if cache is disabled (define YMF_ENABLE_CACHE to enable)")
void ymf_init();
// The ymf_read_reg function is only available when you enable the register
// cache. OPL chips do not provide any means to read current register values.
// Note that enabling the cache increases memory consumption by 512 bytes.
YMF_WARN_NOCACHE("function always returns 0 if cache is disabled (define YMF_ENABLE_CACHE to enable)")
uint8_t ymf_read_reg(uint8_t bank, uint8_t reg);

// The following macros are a symbolic representation of the OPL3 registers
// and timing constants. They are not used internally, but may help with
// programming the OPL.

// sampling frequency (49716Hz @14.318MHz -> chip clock / 288)
#define FM_F_SAMPLE (F_OPL / 288.0)
// tick frequency (=sampling frequency / 4)
#define FM_F_TICK (FM_F_SAMPLE / 4.0)
// long tick = tick / 4
#define FM_F_LONG_TICK (FM_F_TICK / 4.0)
// tick period
#define FM_TICK_TIME (1.0 / FM_F_TICK)

// timer1 -> 80µs tick @14.32MHz
#define FM_TIMER1_VALUE(x) ((uint8_t) (255.0 - (x * FM_F_TICK)))
// timer2 -> 320µs tick @14.32MHz
#define FM_TIMER2_VALUE(x) ((uint8_t) (255.0 - (x * FM_F_LONG_TICK)))

// fnum = ftone * 2^19 / FM_F_SAMPLE / 2^(block-1) = ftone * 2^(20-block) / FM_F_SAMPLE
// according to https://moddingwiki.shikadi.net/wiki/OPL_chip#A0-A8:_Frequency_Number
// the block should be selected as low as possible to ensure maximum accuracy
// TODO
#define FM_FNUM_VALUE(f)
#define FM_FNUML_VALUE(f)
#define FM_FNUMH_VALUE(f)
#define FM_BLOCK_VALUE(f)

// if the register numbers contain both the bank and the address, use these macros
// to call ymf_write_reg with the appropriate 8-bit values
#define FM_BANKOF(x) ((uint8_t) ((x) >> 8))
#define FM_ADDROF(x) ((uint8_t) (x))
#define ymf_write_reg_com(reg, value) ymf_write_reg(FM_BANKOF(reg), FM_ADDROF(reg), value)
#define ymf_read_reg_com(reg) ymf_read_reg(FM_BANKOF(reg), FM_ADDROF(reg))

// register map
#define FM_REG_LSI_TEST1 0x001
#define FM_BIT_WSE (1<<5)
#define FM_REG_TIMER1 0x002
#define FM_REG_TIMER2 0x003
#define FM_REG_RST_MT1_MT2_ST2_ST1 0x004
#define FM_BIT_RST (1<<7)
#define FM_BIT_MT1 (1<<6)
#define FM_BIT_MT2 (1<<5)
#define FM_BIT_ST2 (1<<1)
#define FM_BIT_ST1 (1<<0)
#define FM_REG_NTS 0x008
#define FM_BIT_NTS (1<<6)
// 0x20..0x35
#define FM_REG_AM_VIB_EGT_KSR_MULT_1_0 0x020
#define FM_REG_AM_VIB_EGT_KSR_MULT_1(n) (FM_REG_AM_VIB_EGT_KSR_MULT_1_0 + (n))
#define FM_BIT_AM (1<<7)
#define FM_BIT_VIB (1<<6)
#define FM_BIT_EGT (1<<5)
#define FM_BIT_KSR (1<<4)
#define FM_SHIFT_MULT 0
#define FM_MASK_MULT 0x0f
#define FM_MAKE_MULT(x) (((x) << FM_SHIFT_MULT) & FM_MASK_MULT)
// 0x40..0x55
#define FM_REG_KSL_TL_1_0 0x040
#define FM_REG_KSL_TL_1(n) (FM_REG_KSL_TL_1_0 + (n))
#define FM_SHIFT_KSL 6
#define FM_MASK_KSL 0xc0
#define FM_MAKE_KSL(x) (((x) << FM_SHIFT_KSL) & FM_MASK_KSL)
#define FM_SHIFT_TL 0
#define FM_MASK_TL 0x3f
#define FM_MAKE_TL(x) (((x) << FM_SHIFT_TL) & FM_MASK_TL)
#define FM_MAKE_TL_DB(db) FM_MAKE_TL((uint8_t) ((db) * (1.0 / 0.75)))
// 0x60..0x75
#define FM_REG_AR_DR_1_0 0x060
#define FM_REG_AR_DR_1(n) (FM_REG_AR_DR_1_0 + (n))
#define FM_SHIFT_AR 4
#define FM_MASK_AR 0xf0
#define FM_MAKE_AR(x) (((x) << FM_SHIFT_AR) & FM_MASK_AR)
#define FM_SHIFT_DR 0
#define FM_MASK_DR 0x0f
#define FM_MAKE_DR(x) (((x) << FM_SHIFT_DR) & FM_MASK_DR)
// 0x80..0x95
#define FM_REG_SL_RR_1_0 0x080
#define FM_REG_SL_RR_1(n) (FM_REG_SL_RR_1_0 + (n))
#define FM_SHIFT_SL 4
#define FM_MASK_SL 0xf0
#define FM_MAKE_SL(x) (((x) << FM_SHIFT_AR) & FM_MASK_AR)
#define FM_SHIFT_RR 0
#define FM_MASK_RR 0x0f
#define FM_MAKE_RR(x) (((x) << FM_SHIFT_DR) & FM_MASK_DR)
// 0xa0..0xa8
#define FM_F_NUMBERL_1_0 0x0a0
#define FM_F_NUMBERL_1(n) (FM_F_NUMBERL_1_0 + (n))
#define FM_MAKE_FNUML(x) (x & 0xff)
// 0xb0..0xb8
#define FM_REG_KON_BLOCK_FNUMH_1_0 0x0b0
#define FM_REG_KON_BLOCK_FNUMH_1(n) (FM_REG_KON_BLOCK_FNUMH_1_0 + (n))
#define FM_BIT_KON (1<<5)
#define FM_SHIFT_BLOCK 2
#define FM_MASK_BLOCK 0x1c
#define FM_MAKE_BLOCK(x) (((x) << FM_SHIFT_BLOCK) & FM_MASK_BLOCK)
#define FM_MASK_FNUMH 0x03
#define FM_MAKE_FNUMH(x) (((x) >> 8) & FM_MASK_FNUMH)
#define FM_REG_DAM_DVB_RYT_BD_SD_TOM_TC_HH 0x0bd
#define FM_BIT_DAM (1<<7)
#define FM_BIT_DVB (1<<6)
#define FM_BIT_RYT (1<<5)
#define FM_BIT_BD (1<<4)
#define FM_BIT_SD (1<<3)
#define FM_BIT_TOM (1<<2)
#define FM_BIT_TC (1<<1)
#define FM_BIT_HH (1<<0)
// 0xc0..0xc8
#define FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1_0 0x0c0
#define FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1(n) (FM_REG_CHD_CHC_CHB_CHA_FB_CNT_1_0 + (n))
#define FM_BIT_CHD (1<<7)
#define FM_BIT_CHC (1<<6)
#define FM_BIT_CHB (1<<5)
#define FM_BIT_CHA (1<<4)
#define FM_SHIFT_FB 1
#define FM_MASK_FB 0x0e
#define FM_MAKE_FB(x) (((x) << FM_SHIFT_FB) & FM_MASK_FB)
#define FM_BIT_CNT (1<<0)
// 0xe0..0xf5
#define FM_REG_WS_1_0 0x0e0
#define FM_REG_WS_1(n) (FM_REG_WS_1_0 + (n))

#define FM_REG_LSI_TEST2 0x101
#define FM_REG_CONNECTION_SEL 0x104
#define FM_REG_NEW 0x105
#define FM_BIT_NEW (1<<0)
#define FM_REG_AM_VIB_EGT_KSR_MULT_2_0 0x120
#define FM_REG_AM_VIB_EGT_KSR_MULT_2(n) (FM_REG_AM_VIB_EGT_KSR_MULT_2_0 + (n))
#define FM_REG_KSL_TL_2_0 0x140
#define FM_REG_KSL_TL_2(n) (FM_REG_KSL_TL_2_0 + (n))
#define FM_REG_AR_DR_2_0 0x160
#define FM_REG_AR_DR_2(n) (FM_REG_AR_DR_2_0 + (n))
#define FM_REG_SL_RR_2_0 0x180
#define FM_REG_SL_RR_2(n) (FM_REG_SL_RR_2_0 + (n))
#define FM_F_NUMBERL_2_0 0x1a0
#define FM_F_NUMBERL_2(n) (FM_F_NUMBERL_2_0 + (n))
#define FM_REG_KON_BLOCK_FNUMH_2_0 0x1b0
#define FM_REG_KON_BLOCK_FNUMH_2(n) (FM_REG_KON_BLOCK_FNUMH_2_0 + (n))
#define FM_REG_CHD_CHC_CHB_CHA_FB_CNT_2_0 0x1c0
#define FM_REG_CHD_CHC_CHB_CHA_FB_CNT_2(n) (FM_REG_CHD_CHC_CHB_CHA_FB_CNT_2_0 + (n))
#define FM_REG_WS_2_0 0x1c0
#define FM_REG_WS_2(n) (FM_REG_WS_2_0 + (n))
