/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <util/delay.h>
#include "clock.h"
#include "gpio.h"

#if DT_NOTE_US >= 0x10000 * 1000000 * CLK_DIVISOR / F_CPU
#warning note period out of bounds, increase CLK_DIVISOR
#endif
#if DT_NOTE_US < 100 * 1000000 * CLK_DIVISOR / F_CPU
#warning possibly low clock period precision, decrease CLK_DIVISOR
#endif

static uint16_t clock_last = 0;

void clock_init() {
	// stop timer first
	TCCR1B = 0;
	// reset the elapsed timer
	clock_last = 0;
#if CLK_DIVISOR == 0
	// do nothing, the timer is disabled
#elif CLK_DIVISOR == 1
	TCCR1B = _BV(CS10);
#elif CLK_DIVISOR == 8
	TCCR1B = _BV(CS11);
#elif CLK_DIVISOR == 64
	TCCR1B = _BV(CS11) | _BV(CS10);
#elif CLK_DIVISOR == 256
	TCCR1B = _BV(CS12);
#elif CLK_DIVISOR == 1024
	TCCR1B = _BV(CS12) | _BV(CS10);
#else
#error invalid CLK_DIVISOR
#endif
}

bool clock_elapsed(uint16_t timeout) {
	uint16_t now = TCNT1;
	uint16_t last = clock_last;
	// the difference will wrap around too when TCNT1 wraps around
	uint16_t diff = now - last;
	if (diff >= timeout) {
		// timeout, update the counter
		clock_last = now;
		return true;
	}
	return false;
}

void clock_wait(uint16_t timeout) {
	// TODO this is just for testing, use the code below instead
	//_delay_us(DT_NOTE_US);
	// busy loop until the timer expires
	while (!clock_elapsed(timeout));
}
