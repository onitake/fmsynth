/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CRC_H
#define _CRC_H

#include <stdint.h>

uint8_t sd_crc7_update(uint8_t crc, uint8_t data);
uint16_t sd_crc16_update(uint16_t crc, uint8_t data);
uint16_t sd_crc7(uint8_t *data, uint8_t length);
uint16_t sd_crc16(uint8_t *data, uint16_t length);

#endif //_CRC_H
