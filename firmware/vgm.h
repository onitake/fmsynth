/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _VGM_H
#define _VGM_H

#include <stdbool.h>
#include <stdint.h>

// BCD encode version numbers: 1.51 -> 0x151
#define VGM_MAKE_VERSION(major, tens, ones) ((major << 8) | (tens << 4) | (ones))
// OPL3 et al is supported from VGM 1.51 onwards
#define VGM_MIN_VERSION VGM_MAKE_VERSION(1, 5, 1)

#define VGM_SAMPLE_RATE 44100

#define VGM_MODE_OPL1 10
#define VGM_MODE_OPL2 20
#define VGM_MODE_OPL_MSX 25
#define VGM_MODE_OPL3 30
#define VGM_MODE_OPL4 40

#define VGM_OK 0
#define VGM_ERR_INVALID 1
#define VGM_ERR_IO 2
#define VGM_ERR_UNSUPPORTED 3
#define VGM_ERR_PARAM 99
#define VGM_END 199

typedef uint8_t vgm_err_t;

// read or skip [len] bytes
// pass buf=NULL to skip
typedef bool (*vgm_read_bytes)(uint8_t *buf, uint8_t len);

typedef struct {
	uint32_t version;
	uint32_t data_length;
	uint32_t samples;
	uint32_t loop_offset;
	uint32_t loop_samples;
	uint32_t data_offset;
	uint32_t opl_clock;
	vgm_read_bytes read;
	uint8_t opl_mode;
	uint8_t volume_modifier;
	uint8_t loop_base;
	uint8_t loop_modifier;
} vgm_header_t;

uint8_t vgm_open(vgm_header_t *hdr, vgm_read_bytes read);
vgm_err_t vgm_next(vgm_header_t *hdr);

#endif //_VGM_H
