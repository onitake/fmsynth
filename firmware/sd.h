/*
 *  Copyright © 2021 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SD_H
#define _SD_H

#include <stdint.h>
#include <stdbool.h>

// Error conditions
// General error condition for functions that return R1 or R1b.
// Also indicates response timeout.
#define SD_GENERAL_ERROR 0xff
// General ok status for functions that don't return one of the status bytes.
#define SD_GENERAL_OK 0x00

// Fixed block size
#define SD_BLOCK_SIZE 512

// Card types
#define SD_CARD_TYPE_V1 0x01
#define SD_CARD_TYPE_V2 0x02
#define SD_CARD_TYPE_V2_HCS 0x03

// initializes the SD card control lines, but not the SD card
void sd_init();
// checks if the insertion detector is active
// note that the hardware might not have insertion detection, in which case
// this flag should ignored
bool sd_inserted();

// initializes the SD card
// returns one of the SD_CARD_TYPE values if initialization completed
// successfully and card is ready to accept commands, or SD_GENERAL_ERROR if
// an error occured.
// this function must be executed after a card is inserted, without
// calling sd_select first.
// CS is kept asserted and SPI clock set to high speed mode if card is
// successfully initialized. on error, sd_deselect is called.
uint8_t sd_start();

// All SD communication, except for sd_start(), needs to happen between a
//     sd_select();
//     ...
//     sd_deselect();
// sequence. This enables the level shifter and SD chip select, and sets SPI
// to high speed mode. it then waits until the output drivers are active.
// sd_start() has its own enable/disable sequence and doesn't need a call
// to sd_select.

// enable SD chip select and level shifter, set SPI to high speed mode
void sd_select();
// disable SD chip select and level shifter
void sd_deselect();

// reads a 512-byte data block from the card
// CS must be asserted before calling this function, and card must be initialized
// return SD_GENERAL_OK on success, or SD_GENERAL_ERROR on error
uint8_t sd_read(uint32_t blockid, uint8_t data[SD_BLOCK_SIZE]);

#endif //_SD_H
