/*
 *  Copyright © 2024 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <stddef.h>
#include <avr/pgmspace.h>
#include "vgm.h"
#include "clock.h"
#include "ymf.h"

static const PROGMEM char vgm_magic[] = "Vgm ";

#define VGM_CPU_RATE ((uint32_t) (F_CPU / VGM_SAMPLE_RATE))
#define VGM_SAMPLES_TO_US(samples) ((uint32_t) ((samples) * 1000000.0 / (VGM_SAMPLE_RATE)))

static uint32_t bytes_to_u32le(uint8_t buf[4]) {
	return (uint32_t) buf[0] | ((uint32_t) buf[1] << 8) | ((uint32_t) buf[2] << 16) | ((uint32_t) buf[3] << 24);
}

static uint16_t bytes_to_u16le(uint8_t buf[2]) {
	return (uint8_t) buf[0] | ((uint32_t) buf[1] << 8);
}

static void vgm_wait(uint16_t samples) {
	//printf("W%u|%uus|%uT\n", samples, VGM_SAMPLES_TO_US(samples), CLK_STEPS_US(VGM_SAMPLES_TO_US(samples)));
	// TODO calculate wait timeout in base clock cycles
	// TODO issue multiple wait commands when range exceeded
	// DT_vgm = samples / VGM_SAMPLE_RATE = samples / 44100Hz
	// DT_vgm_min = 1 / VGM_SAMPLE_RATE = 22.6757369614µs
	// DT_vgm_max = 65535 / VGM_SAMPLE_RATE = 1.48605442177s
	// DT_cpu = ticks * CLK_DIVISOR / F_CPU = ticks * 64 / 16MHz
	// DT_cpu_min = 1 * CLK_DIVISOR / F_CPU = 4µs
	// DT_cpu_max = 65535 * CLK_DIVISOR / F_CPU = 65535 * 64 / 16MHz = 262.14ms
	// samples / VGM_SAMPLE_RATE = ticks * CLK_DIVISOR / F_CPU => samples / 44.1kHz = ticks * 64 / 16MHz
	// => samples = ticks * 44.1kHz * 64 / 16MHz = ticks * 0.1764
	// => ticks = samples * 16MHz / 44.1kHz / 64 = samples * 5.66893424036
	// samples_max = DT_cpu_max * VGM_SAMPLE_RATE = 262.14ms * 44100 = 11560.374 ≃ 11560

	// FIXME the following assumes a fixed scaling factor of 6, which is only 5.5% faster
	// samples_max = 65535 / 6 = 10922.5 ≃ 10922
	while (samples > 0) {
		uint16_t ticks;
		if (samples > 10922U) {
			ticks = 10922U * 6;
			samples -= 10922;
		} else {
			ticks = samples * 6;
			samples = 0;
		}
		clock_wait(ticks);
	}
}

vgm_err_t vgm_open(vgm_header_t *hdr, vgm_read_bytes read) {
	if (!hdr || !read) {
		return VGM_ERR_PARAM;
	}
	hdr->read = read;

	uint8_t buf[4];
	// "Vgm " ident
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	for (uint8_t i = 0; i < sizeof(buf); i++) {
		if (buf[i] != (uint8_t) pgm_read_byte((vgm_magic) + i)) {
			return VGM_ERR_INVALID;
		}
	}
	// EoF offset
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->data_length = bytes_to_u32le(buf);
	// Version
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->version = bytes_to_u32le(buf);
	if (hdr->version < VGM_MIN_VERSION) {
		return VGM_ERR_UNSUPPORTED;
	}
	if (!read(NULL, 12)) {
		return VGM_ERR_IO;
	}
	// Total # samples
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->samples = bytes_to_u32le(buf);
	// Loop offset
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->loop_offset = bytes_to_u32le(buf);
	// Loop # samples
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->loop_samples = bytes_to_u32le(buf);
	if (!read(NULL, 16)) {
		return VGM_ERR_IO;
	}
	// VGM data offset
	if (!read(buf, sizeof(buf))) {
		return VGM_ERR_IO;
	}
	hdr->data_offset = bytes_to_u32le(buf);
	if (hdr->data_offset >= 28) {
		if (!read(NULL, 24)) {
			return VGM_ERR_IO;
		}
	}
	uint32_t opl2_clock = 0;
	if (hdr->data_offset >= 32) {
		// YM3812 clock
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		opl2_clock = bytes_to_u32le(buf);
	}
	uint32_t opl1_clock = 0;
	if (hdr->data_offset >= 36) {
		// YM3526 clock
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		opl1_clock = bytes_to_u32le(buf);
	}
	uint32_t opl_msx_clock = 0;
	if (hdr->data_offset >= 40) {
		// Y8950 clock
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		opl_msx_clock = bytes_to_u32le(buf);
	}
	uint32_t opl3_clock = 0;
	if (hdr->data_offset >= 44) {
		// YMF262 clock
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		opl3_clock = bytes_to_u32le(buf);
	}
	uint32_t opl4_clock = 0;
	if (hdr->data_offset >= 48) {
		// YMF278B clock
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		opl4_clock = bytes_to_u32le(buf);
	}
	if (hdr->data_offset >= 64) {
		if (!read(NULL, 12)) {
			return VGM_ERR_IO;
		}
		// VM || *** || LB || LM
		if (!read(buf, sizeof(buf))) {
			return VGM_ERR_IO;
		}
		hdr->volume_modifier = buf[0];
		hdr->loop_base = buf[2];
		hdr->loop_modifier = buf[3];
	} else {
		hdr->volume_modifier = 0;
		hdr->loop_base = 0;
		hdr->loop_modifier = 0;
	}
	if (hdr->data_offset > 64) {
		if (!read(NULL, hdr->data_offset - 64)) {
			return VGM_ERR_IO;
		}
	}

	if (opl4_clock) {
		hdr->opl_mode = VGM_MODE_OPL4;
		hdr->opl_clock = opl4_clock;
	} else if (opl3_clock) {
		hdr->opl_mode = VGM_MODE_OPL3;
		hdr->opl_clock = opl3_clock;
	} else if (opl_msx_clock) {
		hdr->opl_mode = VGM_MODE_OPL_MSX;
		hdr->opl_clock = opl_msx_clock;
	} else if (opl2_clock) {
		hdr->opl_mode = VGM_MODE_OPL2;
		hdr->opl_clock = opl2_clock;
	} else if (opl1_clock) {
		hdr->opl_mode = VGM_MODE_OPL1;
		hdr->opl_clock = opl1_clock;
	} else {
		return VGM_ERR_UNSUPPORTED;
	}

	return VGM_OK;
}

vgm_err_t vgm_next(vgm_header_t *hdr) {
	uint8_t cmd, bank, reg, data;
	uint8_t buf[2];
	if (!hdr->read(&cmd, sizeof(cmd))) {
		return VGM_ERR_IO;
	}
	switch (cmd) {
		case 0x5A:
			// aa dd YM3812, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL2 && hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL_MSX && hdr->opl_mode != VGM_MODE_OPL4) {
				//printf("!>=OPL2\n");
			}
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5B:
			// aa dd YM3526, write value dd to register aa
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5C:
			// aa dd Y8950, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL_MSX) {
				//printf("!=MSX\n");
			}
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5E:
			// aa dd YMF262 port 0, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL4) {
				//printf("!>=OPL3\n");
			}
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(0, reg, data);
			break;
		case 0x5F:
			// aa dd YMF262 port 1, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL3 && hdr->opl_mode != VGM_MODE_OPL4) {
				//printf("!>=OPL3\n");
			}
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(1, reg, data);
			break;
		case 0x61:
			// nn nn Wait n samples, n can range from 0 to 65535 (approx 1.49 seconds). Longer pauses than this are represented by multiple wait commands.
			if (!hdr->read(buf, sizeof(buf))) {
				return VGM_ERR_IO;
			}
			vgm_wait(bytes_to_u16le(buf));
			break;
		case 0x62:
			// wait 735 samples (60th of a second), a shortcut for 0x61 0xdf 0x02
			vgm_wait(735);
			break;
		case 0x63:
			// wait 882 samples (50th of a second), a shortcut for 0x61 0x72 0x03
			vgm_wait(882);
			break;
		case 0x66:
			// end of sound data
			return VGM_END;
		case 0x70:
		case 0x71:
		case 0x72:
		case 0x73:
		case 0x74:
		case 0x75:
		case 0x76:
		case 0x77:
		case 0x78:
		case 0x79:
		case 0x7a:
		case 0x7b:
		case 0x7c:
		case 0x7d:
		case 0x7e:
		case 0x7f:
			// wait n+1 samples, n can range from 0 to 15.
			vgm_wait(cmd - 0x70);
			break;
		case 0xD0:
			// pp aa dd YMF278B, port pp, write value dd to register aa
			if (hdr->opl_mode != VGM_MODE_OPL4) {
				//printf("!=OPL4\n");
			}
			if (!hdr->read(&bank, sizeof(bank))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&reg, sizeof(reg))) {
				return VGM_ERR_IO;
			}
			if (!hdr->read(&data, sizeof(data))) {
				return VGM_ERR_IO;
			}
			ymf_write_reg(bank, reg, data);
			break;
		default:
			//printf("?%02x\n", cmd);
			return VGM_ERR_UNSUPPORTED;
	}
	return VGM_OK;
}
