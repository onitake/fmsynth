/*
 *  Copyright © 2020 Gregor Riepl <onitake@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <avr/io.h>
#include <stdio.h>
#include "uart.h"

#include <util/setbaud.h>

//FILE uart_io = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_io = FDEV_SETUP_STREAM(uart_putchar, uart_getchar, _FDEV_SETUP_RW);

void uart_init() {
	UBRR0 = UBRR_VALUE;
#if USE_2X
	UCSR0A |= _BV(U2X0);
#else
	UCSR0A &= ~_BV(U2X0);
#endif
	UCSR0B |= _BV(TXEN0) | _BV(RXEN0);
	//UCSR0C = _BV(UCSZ01) | _BV(UCSZ00);

	stdout = &uart_io;
	stdin = &uart_io;
	stderr = &uart_io;
}

int uart_putchar(char c, FILE *fp) {
	if (c == '\n') {
		uart_putchar('\r', fp);
	}
	loop_until_bit_is_set(UCSR0A, UDRE0);
	UDR0 = c;
	return 0;
}

int uart_getchar(FILE *fp) {
	loop_until_bit_is_set(UCSR0A, RXC0);
	uint8_t c = UDR0;
	if (c == '\r') {
		c = '\n';
	}
	return c;
}

#if 0
// non-blocking
int uart_putc(char c) {
	if (UCSRA0 & _BV(UDRE0)) {
		UDR = (unsigned char) c;
		return 0
	} else {
		return -1;
	}
}

int uart_puts(char *s) {
	for (; *s; s++) {
		while (uart_putc(*s) != 0);
	}
	return 0;
}

int uart_getc(void) {
	if (UCSRA0 & _BV(RXC0)) {
		return UDR;
	}
	return -1;
}
#endif
