# OPL3 Arduino Shield

Produce FM-synthesized music on your Arduino (Uno)!

## Description

The circuit is based on the Yamaha YMF-262 FM synthesizer chip.
It supports 2 separate output channels (A and B), which are connected to
the left and right audio channel on a stereo jack.

The circuit is based on the example circuits in the YMF-262 (OPL3) and
YAC-512 (DAC) chip data sheets. The two separate outputs are each connected to
a dual operational amplifier that acts as a buffer and an output amplifier.

The output signals are only fed through a simple passive high-pass filter to
remove the 2.5V DC offset. This means that the characteristic staircase
wave form of the DAC is not filtered. If a smoother signal is desired, it must
be filtered externally. Note that even some early sound cards (notably the
original Adlib card) did not include any output filters.

## Design

The YMF-262 has a parallel bus with 8 data lines, two address lines, a read and
write signal and a chip select. Previous versions attempted to connect these
to the SPI port of the ATmega328P through an external shift register, but
this resulted in collisions with the SD card, which also uses the SPI port.
In version 5, this was reverted, and the OPL3 is now connected directly to the
GPIO pins of the microcontroller.

The OPL requires an external oscillator. In practially all original sound
cards, this oscillator is driven by a 14.31818MHz crystal - common back in
the 1980's due to its widespread use for generating NTSC video signals.
It's technically possible to run the OPL from a 16MHz clock signal (which is
what the Arduino Uno uses), but breaking this signal out of the
ATmega328P is difficult, because it requires modifying the fuse bits.
Generating the signal in software or using the PWM outputs is also not possible
due to an upper limit on the frequency (half the system clock).

To avoid these issues and also to save a GPIO pin, an oscillator is included
on the board. This oscillator uses a 14.31818MHz crystal and 74AHC14 Schmitt
inverters to generate a very stable clock signal.

## Memory Card

Due to the limited amount of RAM that the ATmega328P has, a microSD card slot
is included on the synthesizer board.

This slot is connected to the SPI bus through a voltage level converter, which
also acts as a tri-state switch between the bus and the card. A dedicated
chip select line connects the converter's output enable to GPIO pin 8 of of the
Arduino (ATmega328P pin PB0).

The mechanical insertion detection pin of the card slot is connected to
Arduino pin AD0 (PC0) via a pull-up resistor.

As an experiment, a read-only exFAT file system driver is provided. This driver
is optimized for low memory usage, requiring only a 512-byte buffer and some
internal state variables.

Note that the driver is not complete. Most importantly, it is not
case-insensitive as is commonly the case for FAT-type file systems.

## Player Firmware

### Usage

Obviously, you'll need an Arduino Uno (or compatible) board, a microSD card
and a speaker. An external amplifier or active speakers are recommended, but
anything that doesn't put a lot of load on the output is fine.

Make sure the SD card is exFAT formatted - older FAT types will not work.

Plug the shield into the Arduino, the speaker into the headphone jack, the
SD card into the slot on the shield, and an USB cable into your computer.

Then connect to the USB-attached serial port (replace ttyACM1 with the correct
device name for your computer):

```
cu -l /dev/ttyACM1 -s 38400
```

A simple file navigation menu is presented, where folders can be browsed and
files selected for playback. 

### Compilation

This project is designed for the Arduino, but does not use any Arduino
libraries. It does require avr-libc as well as the usual complement of avr-gcc,
avrdude and GNU make.

Connect your Arduino to an USB port and run the following command in a terminal
to build and flash the player firmware:

```shell
cd firmware/
make
make flash
```

This assumes the Arduino was connected as /dev/ttyACM1.
To use a different serial port device, pass the device name:

```shell
make PRG_PORT=/dev/ttyACM0 flash
```

### Supported Media

There are numerous OPL module file formats out there. Practically every DOS
video game used its own player software and custom serialization.

Despite sharing many similarities, it's difficult to implement a player for
every module on an 8-bit microcontroller. Many games used a form of packed
storage, such as compression or separation of instruments, patterns and loops.
This often requires keeping large parts of a module in memory, which is
impossible on a platform with only 4KiB or RAM. The alternative is to reload
data when it's referenced, but this can lead to interruptions during playback.

Therefore, only a handful of formats that are easy to play back are supported:

* Rdos RAW: https://moddingwiki.shikadi.net/wiki/RAW_Format_(Adlib)
* VGM (uncompressed only): https://vgmrips.net/wiki/VGM_Specification

These are not suitable for composing and editing, but can be considered a
continuous stream of write and wait instructions.

Other module types may be supported in the future.

### Internals

Timing is a critical element of electronic music playback. However, the human
brain is not extremely accurate in detecting minor fluctuations, as long as
continuity and harmonics are preserved. In the case of pattern-based tone
generation (as is the case with MIDI or FM synthesizers), having short delays
in note generation (on the order of 1ms or less) won't be noticed. This is
in stark contrast to sample-based audio, where delay of a single sample will
lead to artifacts that are much more noticable.

The player takes advantage of this in several ways:

* Playback speed is not exactly equal to the original. The Arduino uses a
  16MHz clock, which is not easily matched with the 14.31818MHz clock
  of the OPL3. A simplified clock divider constant is used.
  This results in 5% faster playback of Rdos modules.
  Pitch is not affected, because the OPL3 is operated at its native 14.3MHz.
* SD card data is accessed in 512-byte blocks to achieve maximum transfer
  speed. The SPI interface in the ATmega328P has a maximum speed of 8MHz,
  which results in a single block transfer requiring about 600µs. This happens
  in line during playback, and is not noticable.

## Legal

This project is Copyright © 2020-2024 Gregor Riepl <onitake@gmail.com>

Licensed under the CERN Open Hardware License v1.2.
See the LICENSE file for details.

Firmware is licensed under the GNU GPL v3.
See firmware/LICENSE for details.

OPL™ is a trade mark of YAMAHA Corporation. The OPL3 logo used on the PCB is
based on original work copyright © 1998 YAMAHA Corporation.

The exFAT driver was developed based on the open exFAT specification published
by Microsoft®. Certain aspects of exFAT may be covered by patents (which
may or may not be valid), and the author has made an effort to put these into
the separate fat_patented.c C source code file. They are not strictly needed
for a basic read-only file sytem driver, and they are disabled by default.

The open exFAT specification can be found here:
https://learn.microsoft.com/en-us/windows/win32/fileio/exfat-specification

The source code itself is covered by the GNU GPL v3 and may used under the
terms of this license. However, the author can not and will not grant any third
party patent licenses. Use at your own risk.

The simplified SD card interface specification is publicly available from:
https://www.sdcard.org/

However, the simplified specification is incomplete and lacks several sections
essential for communicating with SD cards via the SPI interface.
The author has relied on other information sources on the internet to obtain
the missing information.
